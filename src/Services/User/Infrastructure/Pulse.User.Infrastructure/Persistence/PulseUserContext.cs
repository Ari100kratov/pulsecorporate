﻿using Microsoft.EntityFrameworkCore;
using Pulse.User.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pulse.User.Infrastructure.Persistence
{
    public class PulseUserContext : DbContext, IApplicationContext
    {
        public PulseUserContext(DbContextOptions<PulseUserContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }
    }
}
