﻿using Calendar.API.Application.Abstractions;
using Calendar.API.Domain.Entities;
using Calendar.API.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Calendar.API.Infrastructure.Implementation
{

    /// <summary>
    /// Repository for managing Role entities.
    /// </summary>
    public sealed class UserRepository : BaseRepository<User>, IUserRepository
    {

        /// <summary>
        /// Initializes a new instance of the UserRepository class.
        /// </summary>
        /// <param name="context">The application context.</param>
        public UserRepository(IApplicationContext context) : base(context)
        {
        }

        public async Task<User?> GetByEmailAsync(string email, bool noTracking = true)
        {
            return noTracking ? await _entitySet.AsNoTracking().FirstOrDefaultAsync(x => string.Equals(x.Email, email)) :
                await _entitySet.FirstOrDefaultAsync(x => string.Equals(x.Email, email));
        }

        public async Task<ICollection<User>> GetUsersById(IEnumerable<Guid> ids, bool noTracking = true)
        {
            return noTracking ? await _entitySet.AsNoTracking().Where(u => ids.Contains(u.Id)).AsNoTracking().ToListAsync() :
                await _entitySet.Where(u => ids.Contains(u.Id)).ToListAsync();
        }
    }
}
