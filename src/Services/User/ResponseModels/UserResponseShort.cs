﻿namespace User.API.ResponseModels
{
    internal sealed record UserResponseShort(
        Guid Id,
        string Name,
        string Surname,
        string Email);
}
