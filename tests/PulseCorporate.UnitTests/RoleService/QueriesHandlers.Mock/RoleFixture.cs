﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace tests.PulseCorporate.UnitTests.RoleService.QueriesHandlers.Mock
{
    public class RoleFixture: IDisposable
    {
        public IServiceProvider ServiceProvider { get; set; }
        public RoleFixture() 
        {
            var builder = new ConfigurationBuilder();
            var configuration = builder.Build();
            var serviceCollection = new ServiceCollection().AddServicesTests_Mock(configuration);
            ServiceProvider = serviceCollection.BuildServiceProvider();
        }
        public void Dispose() { }   
    }
}
