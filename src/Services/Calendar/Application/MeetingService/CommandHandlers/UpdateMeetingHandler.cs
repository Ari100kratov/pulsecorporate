﻿using AutoMapper;
using Calendar.API.Application.Abstractions;
using Calendar.API.Application.MeetingService.Commands;
using Calendar.API.Domain.Entities;
using Calendar.API.Domain.EntitiesDto;
using Calendar.API.RabbitMQ.Interfaces;
using MediatR;

namespace Calendar.API.Application.CalendarService.CommandHandlers
{
    public class UpdateMeetingHandler : IRequestHandler<UpdateMeetingAsyncCommand, MeetingDto>
    {
        private readonly IMeetingRepository _meetingRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IRabbitEmailProducer<EmailDto> _rabbit;

        public UpdateMeetingHandler(IMeetingRepository meetingRepository, IUserRepository userRepository, IMapper mapper, IRabbitEmailProducer<EmailDto> rabbit)
        {
            _meetingRepository = meetingRepository;
            _userRepository = userRepository;
            _mapper = mapper;
            _rabbit = rabbit;
        }

        public async Task<MeetingDto> Handle(UpdateMeetingAsyncCommand request, CancellationToken cancellationToken)
        {

            var email = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress")).Value;

            request.Meeting.Id = request.Id;

            var existMeeting = await _meetingRepository.GetByIdAsync(request.Id, false) ??
               throw new KeyNotFoundException($"Not found {nameof(Meeting)} with this id: {request.Id}");
            request.Meeting.ParticipantsId = request.Meeting.ParticipantsId!.Union(new Guid[] { existMeeting.CreatorId });
            request.Meeting.CreatorId = existMeeting.CreatorId;

            _mapper.Map(request.Meeting, existMeeting);

            var participants = await _userRepository.GetUsersById(request.Meeting.ParticipantsId!, false);
            existMeeting.Participants = participants;

            _meetingRepository.Update(existMeeting);
            await _meetingRepository.SaveChangesAsync(cancellationToken);

            _rabbit.Send(new EmailDto(email, "Update meeting", $"Add meeting topic - {request.Meeting.Topic}, description - {request.Meeting.Description}"));

            return _mapper.Map<MeetingDto>(existMeeting);
        }
    }
}
