﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Pulse.Calendar.Infrustructure.Pesrsistence.Configuration
{
    public void Configure(EntityTypeBuilder<User> builder)
    {
        builder.HasKey(x => x.Id);

        builder.Property(x => x.Name)
            .HasMaxLength(50);
        builder.Property(x => x.Surname)
            .HasMaxLength(50);

        builder.Property(x => x.Email)
            .HasMaxLength(255);
    }
}
