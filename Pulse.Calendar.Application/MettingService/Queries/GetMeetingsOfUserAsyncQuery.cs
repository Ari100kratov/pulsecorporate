﻿using MediatR;
using Pulse.Calendar.Domain.EntitiesDto;
using System.Security.Claims;


namespace Pulse.Calendar.Application.MettingService.Queries
{
    public sealed record GetMeetingsOfUserAsyncQuery(IEnumerable<Claim> UserClaims) : IRequest<IEnumerable<MeetingDto>>;
}
