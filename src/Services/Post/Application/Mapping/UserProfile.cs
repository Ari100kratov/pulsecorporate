﻿using AutoMapper;
using Post.API.Domain.Entities;
using Post.API.Domain.EntitiesDto;

namespace Post.API.Application.Mapping
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDto, User>()
                .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember is not null));
            CreateMap<User, UserDto>();
        }
    }
}
