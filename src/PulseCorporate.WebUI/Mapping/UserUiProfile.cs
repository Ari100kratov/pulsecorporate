﻿using AutoMapper;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.WebUI.Models.Administration;
using PulseCorporate.WebUI.ResponseModels.Administration;

namespace PulseCorporate.WebUI.Mapping
{
    public sealed class UserUiProfile : Profile
    {
        public UserUiProfile()
        {
            CreateMap<UserDto, UserResponseShort>();            
            CreateMap<UserModel, UserDto>()
                .ForMember(x => x.Id, map => map.Ignore())
                .ForMember(x => x.Email, map => map.Ignore());
        }
    }
}
