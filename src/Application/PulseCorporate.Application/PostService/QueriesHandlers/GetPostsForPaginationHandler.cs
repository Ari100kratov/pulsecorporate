﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Caching.Distributed;
using PulseCorporate.Application.PostService.Queries;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions.Administration;
using System.Text.Json;

namespace PulseCorporate.Application.PostService.QueriesHandlers
{
    public class GetPostsForPaginationHandler : IRequestHandler<GetPostsForPaginationAsyncQuery, IEnumerable<PostDto>>
    {
        private readonly IPostRepository _postRepository;
        private readonly IMapper _mapper;
        private readonly IDistributedCache _distributedCache;

        public GetPostsForPaginationHandler(IPostRepository postRepository, IMapper mapper, IDistributedCache distributedCache)
        {
            _postRepository = postRepository;
            _mapper = mapper;
            _distributedCache = distributedCache;
        }
        
        public async Task<IEnumerable<PostDto>> Handle(GetPostsForPaginationAsyncQuery request, CancellationToken cancellationToken)
        {
            string serializedPosts = await _distributedCache.GetStringAsync($"posts:{request.Page}:{request.Quantity}");
            if (serializedPosts != null)
            {
                var deserializedPosts = JsonSerializer.Deserialize<IEnumerable<Post>>(serializedPosts);
                return deserializedPosts.Select(_mapper.Map<PostDto>);
            }

            var posts = await _postRepository.GetForPaginationAsync(request.Page, request.Quantity);

            if (posts != null)
            {
                await _distributedCache.SetStringAsync(
                   key: $"posts:{request.Page}:{request.Quantity}",
                   value: JsonSerializer.Serialize(value: posts),
                   options: new DistributedCacheEntryOptions
                   {
                       SlidingExpiration = TimeSpan.FromSeconds(10),
                   });
            } 
            return posts.Select(_mapper.Map<PostDto>);
        }
    }
}
