﻿namespace Post.API.Models.ResponceModel
{
    internal sealed record UserResponseShort(
       Guid Id,
       string Name,
       string Surname,
       string Email);
}
