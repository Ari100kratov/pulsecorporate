﻿using MediatR;
using Post.API.Domain.EntitiesDto;
using System.Security.Claims;

namespace Post.API.Application.PostService.Commands
{
    public record UpdatePostAsyncCommand(Guid Id, PostDto Post, IEnumerable<Claim> UserClaims) : IRequest<PostDto>;
}
