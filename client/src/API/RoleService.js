import axios from "axios";

export default class RoleService {
  static async getAll() {
    const response = await axios.get("http://localhost:5000/api/roles");
    return response;
  }

  static async getById(id) {
    const response = await axios.get(`http://localhost:5000/api/roles${id}`);
    return response;
  }
}