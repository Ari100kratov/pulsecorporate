﻿using AutoFixture;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using PulseCorporate.Application.RoleService.CommandHandlers;
using PulseCorporate.Application.RoleService.Commands;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions.Administration;
using Xunit;

namespace tests.PulseCorporate.UnitTests.RoleService.CommandHandlers.InMemory
{
    public class AddRoleHandlerTests:IClassFixture<RoleFixtureInMemory>
    {
        private IRoleRepository? _roleRepository;
        private Fixture _autoFixture = new Fixture();
        private AddRoleHandler _handler;

        public AddRoleHandlerTests(RoleFixtureInMemory roleFixtureInMemory) 
        {
            var serviceProvider = roleFixtureInMemory.serviceProvider;
            var mapper = serviceProvider.GetService<IMapper>();
            _roleRepository = serviceProvider.GetService<IRoleRepository>();
            _handler = new AddRoleHandler(_roleRepository, mapper);
            
        }

        [Fact]
        public async void Handle_Returns_Collection_With_New_Object()
        {
            //Arrange
            var roleDto = _autoFixture.Create<RoleDto>();
            //Act
            await _handler.Handle(new AddRoleAsyncCommand(roleDto), new CancellationToken());
            //Assert
            var addedRole = await _roleRepository.GetByIdAsync(roleDto.Id);

            Assert.Equal(roleDto.Id, addedRole.Id); 
        }
    }
}
