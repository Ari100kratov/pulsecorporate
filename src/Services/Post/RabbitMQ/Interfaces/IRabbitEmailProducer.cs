﻿namespace Post.API.RabbitMQ.Interfaces
{
    public interface IRabbitEmailProducer<T>
    {
        void Send(T message);
    }
}
