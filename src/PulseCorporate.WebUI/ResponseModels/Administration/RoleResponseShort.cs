﻿using PulseCorporate.Domain.Entities;

namespace PulseCorporate.WebUI.ResponseModels.Administration
{
    internal sealed record RoleResponseShort(
        Guid Id,
        string Name,
        string Description);
}
