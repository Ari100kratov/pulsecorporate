﻿using Serilog.Exceptions.Core;
using Serilog.Exceptions.EntityFrameworkCore.Destructurers;
using Serilog;
using Serilog.Exceptions;
using Serilog.Sinks.Elasticsearch;
using System.Reflection;
using Pulse.User.Infrastructure;
using AutoMapper;
using Pulse.User.Application.Mapping;
using User.API.Mapping;
using MediatR;
using Pulse.User.Repositories.Abstractions.Interfaces;
using Pulse.User.Infrastructure.Implementation;
using Pulse.User.Domain.EntitiesDto;
using Pulse.User.Application.UserService.Queries;
using Pulse.User.Application.UserService.QueriesHandlers;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Pulse.User.Application.UserService.Commands;
using Pulse.User.Application.UserService.CommandHandlers;

namespace User.API
{
    public static class Registrar
    {
        internal static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddSingleton((IConfigurationRoot)configuration)
                //.AddAuth(configuration)
                .AddMediatR(cfg => cfg.RegisterServicesFromAssembly(typeof(Program).Assembly))
                .AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()))
                .AddInfrastructureServices(configuration)
                .InstallHandlers()
                .InstallRepositories();
        }

        #region Auth

        private static IServiceCollection AddAuth(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            const string authPolicyName = "pulse-auth-policy";
            string issuingAuthority = configuration.GetSection("Keycloak").Value!;
            const string validAudience = "account";

            serviceCollection.AddAuthentication().AddJwtBearer(jwtBearerOptions =>
            {
                jwtBearerOptions.Authority = issuingAuthority;
                jwtBearerOptions.RequireHttpsMetadata = false;
                jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = issuingAuthority,
                    ValidAudience = validAudience,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true
                };
            });


            serviceCollection.AddControllers(mvcOptions =>
            {
                mvcOptions.Filters.Add(new AuthorizeFilter(authPolicyName));
            });

            return serviceCollection;
        }

        #endregion

        #region Logging

        public static void ConfigureLogging()
        {
            string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")!;

            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile(
                    $"appsettings.{environment}.json", optional: true
                ).Build();
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .Enrich.WithExceptionDetails(new DestructuringOptionsBuilder()
                    .WithDefaultDestructurers()
                    .WithDestructurers(new[] { new DbUpdateExceptionDestructurer() }))
                .WriteTo.Debug()
                .WriteTo.Console()
                .WriteTo.Elasticsearch(ConfigureElasticSink(configuration, environment))
                .Enrich.WithProperty("Environment", environment)
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }

        private static ElasticsearchSinkOptions ConfigureElasticSink(IConfigurationRoot configuration, string environment)
        {
            return new ElasticsearchSinkOptions(new Uri(configuration["ElastcConfiguration:Uri"]!))
            {
                AutoRegisterTemplate = true,
                IndexFormat = $"{Assembly.GetExecutingAssembly().GetName().Name!.ToLower().Replace(".", "-")}-{environment.ToLower()}-{DateTime.UtcNow:yyyy-MM-dd}",
                NumberOfReplicas = 1,
                NumberOfShards = 2
            };
        }

        #endregion

        private static IServiceCollection InstallHandlers(this IServiceCollection serviceCollection)
        {
            serviceCollection
                //user
                .AddTransient<IRequestHandler<GetUserByEmailAsyncQuery, UserDto>, GetUserByEmailHandler>()
                .AddTransient<IRequestHandler<GetUsersAsyncQuery, IEnumerable<UserDto>>, GetUsersHandler>()
                .AddTransient<IRequestHandler<UpdateUserAsyncCommand, UserDto>, UpdateUserHandler>()
                .AddTransient<IRequestHandler<AddUserAsyncCommand, UserDto>, AddUserHandler>();
            return serviceCollection;
        }

        private static IServiceCollection InstallRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection                
                .AddTransient<IUserRepository, UserRepository>();
            return serviceCollection;
        }

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {                
                //user
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<UserUiProfile>();                
            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }
    }
}
