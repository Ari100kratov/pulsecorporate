import Home from "../pages/Home";
import Error from "../pages/Error";
import Support from '../pages/Support';
import { CalendarPage } from "../pages/Calendar";

export const privateRoutes = [
	{ path: "/support", component: Support, exact: true },
	//{ path: "/about", component: About, exact: true },
	//{ path: "/roles", component: Roles, exact: true },
	//{ path: "/roles/:id", component: PostIdPage, exact: true },
	{ path: "/calendar", component: CalendarPage, exact: true },
	{ path: "/home", component: Home, exact: true },
	{ path: "/", component: Home, exact: true },
	{ path: "/error", component: Error, exact: true },
	{ path: "/*", component: Error, exact: true },
];

export const publicRoutes = [
	{ path: "/", component: Home, exact: true },
	//{ path: "/signin", component: SignIn, exact:true},
	//{ path: "/signup", component: SignUp, exact: true },
	{ path: "/support", component: Support, exact: true },
	{ path: "/*", component: Home, exact: true },];
