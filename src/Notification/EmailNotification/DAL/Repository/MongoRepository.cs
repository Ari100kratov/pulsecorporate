﻿using EmailNotification.DAL.Interfaces;
using MongoDB.Bson;
using MongoDB.Driver;
using Serilog;

namespace EmailNotification.DAL.Repository
{
    internal class MongoRepository<T> : IRepository<T>
    {
        private IMongoCollection<BsonDocument> _mongoCollection;
        public MongoRepository(IMongoCollection<BsonDocument> mongoCollection) 
        {
            _mongoCollection = mongoCollection;
        }
        public async Task AddToMongo(T item)
        {
            var documents = await _mongoCollection.Find(new BsonDocument()).ToListAsync();
            foreach (var document in documents) 
            {
                Log.Information(document.ToJson());
            }
            var bsonDocument = item.ToBsonDocument();
            await _mongoCollection.InsertOneAsync(bsonDocument);
        }
    }
}
