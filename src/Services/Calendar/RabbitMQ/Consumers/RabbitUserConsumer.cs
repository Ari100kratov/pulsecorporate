﻿using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System.Text;
using System.Text.Json;
using Calendar.API.Domain.Entities;
using Calendar.API.Domain.EntitiesDto;
using Calendar.API.Application.Abstractions;
using AutoMapper;

namespace Calendar.API.RabbitMQ.Consumers
{
    public class RabbitUserConsumer:BackgroundService
    {
        private readonly IConnection _connection;
        private readonly ConnectionFactory _connectionFactory;
        private readonly IModel _model;
        private readonly IConfiguration _configuration;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public RabbitUserConsumer(IServiceScopeFactory scopeFactory)
        {

            using var scope = scopeFactory.CreateScope();
            _userRepository = scope.ServiceProvider.GetRequiredService<IUserRepository>();

            _mapper = scope.ServiceProvider.GetRequiredService<IMapper>();

            _configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();
            _connectionFactory = new ConnectionFactory() { HostName = _configuration["RabbitMQServices:HostName"], UserName = _configuration["RabbitMQServices:UserName"], Password = _configuration["RabbitMQServices:Password"] };
            _connection = _connectionFactory.CreateConnection();
            _model = _connection.CreateModel();
            _model.QueueDeclare(queue: _configuration["RabbitMQServices:QueueNameUser"],
                               durable: false,
                               exclusive: false,
                               autoDelete: false,
                               arguments: null);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_model);
            consumer.Received += (ch, ea) =>
            {
                var body = ea.Body.ToArray();
                var userDto = JsonSerializer.Deserialize<UserDto>(Encoding.UTF8.GetString(body));
                _userRepository.Add(_mapper.Map<User>(userDto));
                _userRepository.SaveChangesAsync();

                _model.BasicAck(ea.DeliveryTag, false);
            };

            _model.BasicConsume(queue: _configuration["RabbitMQServices:QueueNameUser"],
                                                autoAck: false,
                                                consumer: consumer);

            return Task.CompletedTask;
        }
    }
}
