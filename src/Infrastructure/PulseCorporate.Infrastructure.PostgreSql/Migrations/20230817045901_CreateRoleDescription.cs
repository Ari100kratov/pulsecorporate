﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PulseCorporate.Infrastructure.PostgreSql.Migrations
{
    /// <inheritdoc />
    public partial class CreateRoleDescription : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Role",
                type: "text",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "Role");
        }
    }
}
