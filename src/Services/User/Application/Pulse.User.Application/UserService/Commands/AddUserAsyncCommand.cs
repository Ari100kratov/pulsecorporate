﻿using MediatR;
using Pulse.User.Domain.EntitiesDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulse.User.Application.UserService.Commands
{
    public record AddUserAsyncCommand(UserDto User) : IRequest<UserDto>;
}
