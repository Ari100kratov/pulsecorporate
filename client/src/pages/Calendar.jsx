import React, { useState, useEffect } from "react";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { Calendar, momentLocalizer } from "react-big-calendar";
import { useKeycloak } from "@react-keycloak/web";
import { createAxiosInstance } from "../API/CreateAxios";
import moment from "moment";
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  DialogActions,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
} from "@mui/material";
import { TimePicker } from "@mui/lab";
const localizer = momentLocalizer(moment);

export const CalendarPage = () => {
  const { keycloak, initialized } = useKeycloak();
  const [meetings, setMeetings] = useState([]);
  const [users, setUsers] = useState([]);
  const [modalOpen, setModalOpen] = useState(false);
  const [newMeeting, setNewMeeting] = useState({
    id: null,
    title: "",
    start: new Date(),
    end: new Date(),
    desc: "",
    users: [],
    author: "",
  });
  const [currentUser, setCurrentUser] = useState(null);

  const fetchMeetings = async () => {
    const axiosInstance = createAxiosInstance(keycloak);
    const response = await axiosInstance.get(`/Meetings`);
    const meetingsData = response.data;
    setMeetings(
      meetingsData.map((meeting) => ({
        id: meeting.id,
        title: meeting.topic,
        start: new Date(meeting.startTime),
        end: new Date(meeting.endTime),
        desc: meeting.description,
        users: meeting.participants.map((participant) => ({
          id: participant.id,
          name: `${participant.name} ${participant.surname}`,
          email: participant.email,
        })),
        author: `${meeting.creator.name} ${meeting.creator.surname}`,
      }))
    );
  };

  const fetchUsers = async () => {
    const axiosInstance = createAxiosInstance(keycloak);
    const response = await axiosInstance.get(`/Users/AllUsers`);
    const usersData = response.data;
    setUsers(
      usersData.map((user) => ({
        id: user.id,
        name: `${user.name} ${user.surname}`,
        email: user.email,
      }))
    );
  };

  const fetchCurrentUser = async () => {
    const axiosInstance = createAxiosInstance(keycloak);
    try {
      const response = await axiosInstance.get(`/Users/CurrentUser`);
      setCurrentUser(response.data);
    } catch (error) {
      console.error("Error fetching current user", error);
    }
  };

  useEffect(() => {
    fetchCurrentUser();
    fetchUsers();
    fetchMeetings();
  }, []);

  const handleSelectSlot = ({ start, end }) => {
    setNewMeeting({
      ...newMeeting,
      start,
      end,
      author: `${currentUser.name} ${currentUser.surname}`,
    });
    setModalOpen(true);
  };

  const handleSelectEvent = (event) => {
    setNewMeeting({
      ...event,
      users: event.users.map((user) => user.id),
    });
    setModalOpen(true);
  };

  const handleAddOrUpdateMeeting = async () => {
    const updatedMeeting = {
      ...newMeeting,
      author: newMeeting.id
        ? newMeeting.author
        : `${currentUser.name} ${currentUser.surname}`,
    };
    const axiosInstance = createAxiosInstance(keycloak);

    if (newMeeting.id) {
      try {
        const response = await axiosInstance.put(
          `/Meetings/${updatedMeeting.id}`,
          {
            topic: updatedMeeting.title,
            description: updatedMeeting.desc,
            startTime: updatedMeeting.start,
            endTime: updatedMeeting.end,
            participantsId: updatedMeeting.users.map((m) => m),
          }
        );
        setMeetings(
          meetings.map((m) =>
            m.id === newMeeting.id
              ? {
                  id: response.data.id,
                  title: response.data.topic,
                  start: new Date(response.data.startTime),
                  end: new Date(response.data.endTime),
                  desc: response.data.description,
                  users: response.data.participants.map((participant) => ({
                    id: participant.id,
                    name: `${participant.name} ${participant.surname}`,
                    email: participant.email,
                  })),
                  author: `${response.data.creator.name} ${response.data.creator.surname}`,
                }
              : m
          )
        );
      } catch (error) {
        console.error("Error fetching update meetings", error);
      }
    } else {
      try {
        const response = await axiosInstance.post(`/Meetings`, {
          topic: updatedMeeting.title,
          description: updatedMeeting.desc,
          startTime: updatedMeeting.start,
          endTime: updatedMeeting.end,
          participantsId: updatedMeeting.users.map((m) => m),
        });
        setMeetings([
          ...meetings,
          {
            id: response.data.id,
            title: response.data.topic,
            start: new Date(response.data.startTime),
            end: new Date(response.data.endTime),
            desc: response.data.description,
            users: response.data.participants.map((participant) => ({
              id: participant.id,
              name: `${participant.name} ${participant.surname}`,
              email: participant.email,
            })),
            author: `${response.data.creator.name} ${response.data.creator.surname}`,
          },
        ]);
      } catch (error) {
        console.error("Error fetching new meetings", error);
      }
    }
    handleClose();
  };

  const handleDeleteMeeting = async () => {
    const axiosInstance = createAxiosInstance(keycloak);
    await axiosInstance.delete(`/Meetings/${newMeeting.id}`);
    setMeetings(meetings.filter((meeting) => meeting.id !== newMeeting.id));
    handleClose();
  };

  const handleClose = () => {
    setModalOpen(false);
    setNewMeeting({
      id: null,
      title: "",
      start: new Date(),
      end: new Date(),
      desc: "",
      users: [],
    });
  };

  if (!initialized) {
    return <></>;
  }

  return (
    <>
      <Calendar
        localizer={localizer}
        events={meetings}
        startAccessor="start"
        endAccessor="end"
        style={{ height: 500 }}
        selectable
        onSelectSlot={handleSelectSlot}
        onSelectEvent={handleSelectEvent}
      />
      <Dialog open={modalOpen} onClose={handleClose}>
        <DialogTitle>
          {newMeeting.id ? "Edit Meeting" : "Add New Meeting"}
        </DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            label="Meeting Title"
            type="text"
            fullWidth
            variant="standard"
            value={newMeeting.title}
            onChange={(e) =>
              setNewMeeting({ ...newMeeting, title: e.target.value })
            }
          />
          <TimePicker
            label="Start Time"
            value={newMeeting.start}
            onChange={(newValue) => {
              setNewMeeting({ ...newMeeting, start: newValue });
            }}
          />
          <TextField
            margin="dense"
            label="Description"
            type="text"
            fullWidth
            variant="standard"
            value={newMeeting.desc}
            onChange={(e) =>
              setNewMeeting({ ...newMeeting, desc: e.target.value })
            }
          />
          <FormControl fullWidth>
            <InputLabel id="user-select-label">Users</InputLabel>
            <Select
              labelId="user-select-label"
              multiple
              value={newMeeting.users}
              onChange={(e) =>
                setNewMeeting({ ...newMeeting, users: e.target.value })
              }
            >
              {users.map((user) => (
                <MenuItem key={user.id} value={user.id}>
                  {user.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <TextField
            margin="dense"
            label="Author"
            type="text"
            fullWidth
            variant="standard"
            value={newMeeting.author}
            disabled
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleAddOrUpdateMeeting} color="primary">
            {newMeeting.id ? "Update" : "Add"}
          </Button>
          {newMeeting.id && (
            <Button onClick={handleDeleteMeeting} color="secondary">
              Delete
            </Button>
          )}
        </DialogActions>
      </Dialog>
    </>
  );
};
