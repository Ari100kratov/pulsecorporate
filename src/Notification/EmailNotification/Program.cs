﻿using RabbitMQService.Interfaces;
using RabbitMQService.Services;
using Serilog;
using EmailNotification.DAL.EntityDto;

namespace EmailNotification;
class Program
{
    static void Main(string[] args)
    {
        Registrar.ConfigureLogging();

        Log.Information("Start email service");
        try
        {   
           var services = Registrar.ConfigureServices();

            IRabbitMQConsumer<EmailDto> rabbit = new RabbitMQConsumer<EmailDto>();

            rabbit.Consume(services);

            Console.ReadLine();
        }
        catch (Exception ex)
        {
            Log.Error(ex, "An error occurred.");
        }
        finally
        {            
            Log.CloseAndFlush();
        }
    }
}
