﻿using AutoFixture;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using PulseCorporate.Application.RoleService.CommandHandlers;
using PulseCorporate.Application.RoleService.Commands;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Repositories.Abstractions.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace tests.PulseCorporate.UnitTests.RoleService.CommandHandlers.InMemory
{
    public class DeleteRoleHandlerTests:IClassFixture<RoleFixtureInMemory>
    {
        private DeleteRoleHandler _handle;
        private IRoleRepository _roleRepository;
        private Fixture _autoFixture = new Fixture();
        public DeleteRoleHandlerTests(RoleFixtureInMemory roleFixtureInMemory) 
        {
            var serviceProvider = roleFixtureInMemory.serviceProvider;
            _roleRepository = serviceProvider.GetService<IRoleRepository>();
            _handle = new DeleteRoleHandler(_roleRepository);
        }
        [Fact]
        public async void Handle_Returns_Collection_Without_One_Element()
        {
            //Arrange
            var newRole = _autoFixture.Create<Role>();
            _roleRepository.Add(newRole);
            await _roleRepository.SaveChangesAsync();
            var roles = await _roleRepository.GetAllAsync();
            //Act
            var deletedGuid = await _handle.Handle(new DeleteRoleAsyncCommand(newRole.Id), new CancellationToken());
            //Assert
            var withoutRoles = await _roleRepository.GetAllAsync();
            Assert.DoesNotContain(newRole, withoutRoles);
        }
        [Fact]
        public async void Handle_Returns_KeyNotFoundException_If_Unknown_Id()
        {
            //Arrange
            var unknownGuid = new Guid("a25c4e60-293d-41b7-b9a3-07d0ffec5cf2");

            //Act
            var exception = await Assert.ThrowsAsync<KeyNotFoundException>(async () =>
            await _handle.Handle(new DeleteRoleAsyncCommand(unknownGuid), new CancellationToken()));

            //Assert
            Assert.Equal($"Not found {nameof(Role)} with this id: {unknownGuid}", exception.Message);
        }
    }
}
