﻿using AutoMapper;
using MediatR;
using Pulse.Calendar.Application.MettingService.Queries;
using Pulse.Calendar.Domain.EntitiesDto;
using Pulse.Calendar.Repositories.Abstractions;

namespace Pulse.Calendar.Application.MettingService.QueriesHandler
{
    public class GetMeetingsOfUserHandler : IRequestHandler<GetMeetingsOfUserAsyncQuery, IEnumerable<MeetingDto>>
    {
        private readonly IMeetingRepository _meetingRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public GetMeetingsOfUserHandler(IMeetingRepository meetingRepository, IUserRepository userRepository, IMapper mapper)
        {
            _mapper = mapper;
            _meetingRepository = meetingRepository;
            _userRepository = userRepository;
        }

        public async Task<IEnumerable<MeetingDto>> Handle(GetMeetingsOfUserAsyncQuery request, CancellationToken cancellationToken)
        {
            var email = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress")).Value;

            var existUser = await _userRepository.GetByEmailAsync(email);

            if (existUser is null)
            {
                var name = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname")).Value;
                var surName = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname")).Value;
                _userRepository.Add(new User { Name = name, Surname = surName, Email = email });
                await _userRepository.SaveChangesAsync(cancellationToken);

                existUser = await _userRepository.GetByEmailAsync(email);
            }

            return _mapper.Map<IEnumerable<MeetingDto>>(await _meetingRepository.GetMeetingsOfUserAsync(existUser!));
        }
    }
}
