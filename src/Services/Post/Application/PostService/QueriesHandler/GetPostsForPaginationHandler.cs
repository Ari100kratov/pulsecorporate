﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Caching.Distributed;
using Post.API.Application.Abstractions;
using Post.API.Application.PostService.Queries;
using Post.API.Domain.EntitiesDto;
using System.Text.Json;
using Post.API.Domain.Entities;

namespace Post.API.Application.PostService.QueriesHandler
{
    public class GetPostsForPaginationHandler : IRequestHandler<GetPostsForPaginationAsyncQuery, IEnumerable<PostDto>>
    {
        private readonly IPostRepository _postRepository;
        private readonly IMapper _mapper;
        private readonly IDistributedCache _distributedCache;

        public GetPostsForPaginationHandler(IPostRepository postRepository, IMapper mapper, IDistributedCache distributedCache)
        {
            _postRepository = postRepository;
            _mapper = mapper;
            _distributedCache = distributedCache;
        }

        public async Task<IEnumerable<PostDto>> Handle(GetPostsForPaginationAsyncQuery request, CancellationToken cancellationToken)
        {
            string serializedPosts = await _distributedCache.GetStringAsync($"posts:{request.Page}:{request.Quantity}");
            if (serializedPosts != null)
            {
                var deserializedPosts = JsonSerializer.Deserialize<IEnumerable<Posts>>(serializedPosts);
                return deserializedPosts.Select(_mapper.Map<PostDto>);
            }

            var posts = await _postRepository.GetForPaginationAsync(request.Page, request.Quantity);

            if (posts != null)
            {
                await _distributedCache.SetStringAsync(
                   key: $"posts:{request.Page}:{request.Quantity}",
                   value: JsonSerializer.Serialize(value: posts),
                   options: new DistributedCacheEntryOptions
                   {
                       SlidingExpiration = TimeSpan.FromSeconds(10),
                   });
            }
            return posts.Select(_mapper.Map<PostDto>);
        }
    }
}
