﻿using PulseCorporate.Domain.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Domain.Entities
{
    public class Post : BaseEntity
    {
        public required string Name { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedDate { get; set; }

        public required Guid AuthorId { get; set; }
        public required User Author { get; set; }
    }
}
