using AutoMapper;
using Calendar.API;
using Calendar.API.Application.Abstractions;
using Calendar.API.Infrastructure.ServiceInfrastructure;
using Calendar.API.Middleware;
using Calendar.API.RabbitMQ.Consumers;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddServices(builder.Configuration);
builder.Services.AddControllers();

builder.Services.AddSwaggerGen();

builder.Services.AddHostedService<RabbitUserConsumer>();

Registrar.ConfigureLogging();
builder.Host.UseSerilog();

var app = builder.Build();

app.UseAuthentication();
app.UseAuthorization();

app.UseSwagger();
app.UseSwaggerUI();

app.UseLoggingMiddleware();
app.UseExceptionHandlerMiddleware();

app.MapControllers();

app.Services.InitializeInfrastructureServices();

app.Run();
