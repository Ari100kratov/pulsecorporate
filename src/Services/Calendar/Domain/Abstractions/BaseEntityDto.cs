﻿namespace Calendar.API.Domain.Abstractions
{
    public abstract class BaseEntityDto
    {
        public Guid Id { get; set; }
    }
}
