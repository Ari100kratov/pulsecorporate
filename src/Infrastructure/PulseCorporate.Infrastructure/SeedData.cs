﻿using PulseCorporate.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Infrastructure
{
    /// <summary>
    /// Gets the initial roles for seed data.
    /// </summary>
    internal static class SeedData
    {
        public static IEnumerable<Role> Roles { get; }
        public static IEnumerable<Post> Posts { get; }
        public static IEnumerable<User> Users { get; }

        static SeedData()
        {
            Roles = new List<Role>()
            {
                new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Administrator",
                Description = "System administrator with full access and control.",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "Employee",
                Description = "Company employee with standard permissions."
            },
            new Role()
            {
                Id = Guid.Parse("73745220-8b23-445c-83b1-ae3662dce2b2"),
                Name = "Guest",
                Description = "Limited access guest account."
            }};
            //Posts = new List<Post>()
            //{
            //    new Post()
            //    {
            //        Id=Guid.Parse("bf89b965-3efb-40f5-ace3-895f86aaf1c7"),
            //        Name="Post1",
            //        Description="MyPuss",
            //        CreatedDate= new DateTime(),
            //    },
            //    new Post()
            //    {
            //        Id=Guid.Parse("c7a490b3-b458-4560-9c15-ca557a234cdb"),
            //        Name="Post2",
            //        Description="Elbrus",
            //        CreatedDate=new DateTime()
            //    },
            //    new Post()
            //    {
            //        Id=Guid.Parse("e9e9c58e-c7b0-4c9b-bd1d-845b318032a9"),
            //        Name="Post3",
            //        Description="Me",
            //        CreatedDate=new DateTime()
            //    },
            //    new Post()
            //    {
            //        Id=Guid.Parse("90189aa0-3086-4d5b-8283-79dc90b91c95"),
            //        Name="Post4",
            //        Description="My family",
            //        CreatedDate=new DateTime()
            //    },
            //};
            //Users = new List<User>()
            //{
            //    new User()
            //    {
            //        Id=Guid.Parse("e8fb02c8-5bd3-49f6-9503-360e8854594b"),
            //        Name="Ilya",
            //        Surname="Gurevich",
            //        Email="ilya_gurevich@yandex.ru",
            //        Password="12345",
            //        RoleId=Roles.FirstOrDefault(x=>x.Name=="Administrator").Id
            //    },
            //    new User()
            //    {
            //        Id=Guid.Parse("3735f9bf-7d2d-4cb3-8a14-202073d18d00"),
            //        Name="Vladimir",
            //        Surname="Gnatyuk",
            //        Email="vladimir_gnatyuk@yandex.ru",
            //        Password="12345",
            //        RoleId=Roles.FirstOrDefault(x=>x.Name=="Administrator").Id
            //    },
            //    new User()
            //    {
            //        Id=Guid.Parse("7b2cc276-b97d-4651-9215-b77ac69579e8"),
            //        Name="Alex",
            //        Surname="Trofimov",
            //        Email="alex_trofimov@yandex.ru",
            //        Password="12345",
            //        RoleId=Roles.FirstOrDefault(x=>x.Name=="Administrator").Id
            //    },
            //    new User()
            //    {
            //        Id=Guid.Parse("e8fb02c8-5bd3-49f6-9503-360e8854594b"),
            //        Name="Vlad",
            //        Surname="Sergeev",
            //        Email="vladislove@yandex.ru",
            //        Password="12345",
            //        RoleId=Roles.FirstOrDefault(x=>x.Name=="Administrator").Id
            //    },
            //};
        }
    }
}
