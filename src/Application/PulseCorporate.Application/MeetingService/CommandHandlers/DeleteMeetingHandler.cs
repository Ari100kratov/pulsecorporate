﻿using MediatR;
using PulseCorporate.Application.MeetingService.Commands;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions.Administration;
using RabbitMQService.Interfaces;

namespace PulseCorporate.Application.MeetingService.CommandHandlers
{
    public class DeleteMeetingHandler : IRequestHandler<DeleteMeetingAsyncCommand, Guid>
    {
        private readonly IMeetingRepository _meetingRepository;
        private readonly IRabbitMQProducer<EmailDto> _rabbit;

        public DeleteMeetingHandler(IMeetingRepository meetingRepository, IRabbitMQProducer<EmailDto> rabbit)
        {
            _meetingRepository = meetingRepository;
            _rabbit = rabbit;
        }

        public async Task<Guid> Handle(DeleteMeetingAsyncCommand request, CancellationToken cancellationToken)
        {
            var email = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress")).Value;

            var existMeeting = await _meetingRepository.GetByIdAsync(request.Id) ??
                throw new KeyNotFoundException($"Not found {nameof(Meeting)} with this id: {request.Id}");

            _meetingRepository.Delete(existMeeting);
            await _meetingRepository.SaveChangesAsync(cancellationToken);

            _rabbit.SendMessage(new EmailDto(email, "Delete meeting", $"Delete meeting id - {request.Id}"));

            return request.Id;
        }
    }
}
