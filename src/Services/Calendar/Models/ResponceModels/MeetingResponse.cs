﻿namespace Calendar.API.Models.ResponceModels
{
    internal sealed record MeetingResponse(
       Guid Id,
       string Topic,
       string Description,
       DateTime StartTime,
       DateTime EndTime,
       UserResponseShort Creator,
       IEnumerable<UserResponseShort> Participants);
}
