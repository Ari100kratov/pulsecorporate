import React from "react";
import { Box, LinearProgress, Typography } from "@mui/material";

const Loader = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        height: "100vh", // Занимает всю высоту экрана
      }}
    >
      <Typography variant="h6" color="textSecondary" sx={{ mb: 2 }}>
        Loading...
      </Typography>
      <LinearProgress sx={{ width: "10%", height: "4px" }} />
    </Box>
  );
};

export default Loader;
