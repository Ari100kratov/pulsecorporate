﻿using AutoMapper;
using PulseCorporate.Application.Mapping;
using PulseCorporate.Repositories.Abstractions.Administration;
using PulseCorporate.Repositories.Implementation.Administration;
using PulseCorporate.WebUI.Mapping;
using MediatR;
using PulseCorporate.Application.RoleService.Queries;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Application.RoleService.QueriesHandlers;
using PulseCorporate.Application.RoleService.Commands;
using PulseCorporate.Application.RoleService.CommandHandlers;
using PulseCorporate.Infrastructure;
using PulseCorporate.Application.PostService.QueriesHandlers;
using PulseCorporate.Application.PostService.Queries;
using PulseCorporate.Application.PostService.CommandHandlers;
using PulseCorporate.Application.PostService.Commands;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using PulseCorporate.Application.UserService.Queries;
using PulseCorporate.Application.UserService.QueriesHandlers;
using PulseCorporate.Application.UserService.Commands;
using PulseCorporate.Application.UserService.CommandHandlers;

using RabbitMQService.Interfaces;
using RabbitMQService.Services;
using Microsoft.AspNetCore.Mvc.Authorization;
using Serilog.Sinks.Elasticsearch;
using Serilog;
using System.Reflection;
using Serilog.Exceptions;
using Serilog.Exceptions.Core;
using Serilog.Exceptions.EntityFrameworkCore.Destructurers;
using PulseCorporate.Application.MeetingService.Queries;
using PulseCorporate.Application.MeetingService.QueriesHandlers;
using PulseCorporate.Application.MeetingService.Commands;
using PulseCorporate.Application.MeetingService.CommandHandlers;

namespace PulseCorporate.WebUI
{
    public static class Registrar
    {
        internal static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddSingleton((IConfigurationRoot)configuration)
                .AddAuth(configuration)
                .AddMediatR(cfg => cfg.RegisterServicesFromAssembly(typeof(Program).Assembly))
                .AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()))
                .AddInfrastructureServices(configuration)
                .InstallHandlers()
                .InstallRepositories()
                .InstallRabbitMQServices();
            //.InstallNotificationServices();
        }
       
        private static IServiceCollection InstallHandlers(this IServiceCollection serviceCollection)
        {
            serviceCollection
                //role
                .AddTransient<IRequestHandler<GetRolesAsyncQuery, IEnumerable<RoleDto>>, GetRolesHandler>()
                .AddTransient<IRequestHandler<GetRoleByIdAsyncQuery, RoleDto>, GetRoleByIdHandler>()
                .AddTransient<IRequestHandler<AddRoleAsyncCommand, RoleDto>, AddRoleHandler>()
                .AddTransient<IRequestHandler<UpdateRoleAsyncCommand, RoleDto>, UpdateRoleHandler>()
                .AddTransient<IRequestHandler<DeleteRoleAsyncCommand, Guid>, DeleteRoleHandler>()
                //post
                .AddTransient<IRequestHandler<GetPostsAsyncQuery, IEnumerable<PostDto>>, GetPostsHandler>()
                .AddTransient<IRequestHandler<GetPostsForPaginationAsyncQuery, IEnumerable<PostDto>>, GetPostsForPaginationHandler>()
                .AddTransient<IRequestHandler<GetPostByIdAsyncQuery, PostDto>, GetPostByIdHandler>()
                .AddTransient<IRequestHandler<AddPostAsyncCommand, PostDto>, AddPostHandler>()
                .AddTransient<IRequestHandler<UpdatePostAsyncCommand, PostDto>, UpdatePostHandler>()
                .AddTransient<IRequestHandler<DeletePostAsyncCommand, Guid>, DeletePostHandler>()
                //user
                .AddTransient<IRequestHandler<GetUserByEmailAsyncQuery, UserDto>, GetUserByEmailHandler>()
                .AddTransient<IRequestHandler<GetUsersAsyncQuery, IEnumerable<UserDto>>, GetUsersHandler>()
                .AddTransient<IRequestHandler<UpdateUserAsyncCommand, UserDto>, UpdateUserHandler>()
                //meeting
                .AddTransient<IRequestHandler<GetMeetingsOfUserAsyncQuery, IEnumerable<MeetingDto>>, GetMeetingsOfUserHandler>()
                .AddTransient<IRequestHandler<AddMeetingAsyncCommand, MeetingDto>, AddMeetingHandler>()
                .AddTransient<IRequestHandler<UpdateMeetingAsyncCommand, MeetingDto>, UpdateMeetingHandler>()
                .AddTransient<IRequestHandler<DeleteMeetingAsyncCommand, Guid>, DeleteMeetingHandler>();
            return serviceCollection;
        }

        private static IServiceCollection InstallRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddTransient<IRoleRepository, RoleRepository>()
                .AddTransient<IPostRepository, PostRepository>()
                .AddTransient<IUserRepository, UserRepository>()
                .AddTransient<IMeetingRepository, MeetingRepository>();
            return serviceCollection;
        }

        #region Auth

        private static IServiceCollection AddAuth(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            const string authPolicyName = "pulse-auth-policy";
            string issuingAuthority = configuration.GetSection("Keycloak").Value!;
            const string validAudience = "account";

            serviceCollection.AddAuthentication().AddJwtBearer(jwtBearerOptions =>
            {
                jwtBearerOptions.Authority = issuingAuthority;
                jwtBearerOptions.RequireHttpsMetadata = false;
                jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = issuingAuthority,
                    ValidAudience = validAudience,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true
                };
            });

            serviceCollection.AddAuthorization(authorizationOptions =>
            {
                authorizationOptions.AddPolicy(authPolicyName, policy =>
                {
                    policy.AuthenticationSchemes.Add(JwtBearerDefaults.AuthenticationScheme);
                    policy.RequireAuthenticatedUser();
                });
            });

            serviceCollection.AddControllers(mvcOptions =>
            {
                mvcOptions.Filters.Add(new AuthorizeFilter(authPolicyName));
            });

            return serviceCollection;
        }
        #endregion
        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                //role
                cfg.AddProfile<RoleProfile>();
                cfg.AddProfile<RoleUiProfile>();
                //post
                cfg.AddProfile<PostProfile>();
                cfg.AddProfile<PostUiProfile>();
                //user
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<UserUiProfile>();
                //meeting
                cfg.AddProfile<MeetingProfile>();
                cfg.AddProfile<MeetingUiProfile>();
            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }
        private static IServiceCollection InstallRabbitMQServices(this IServiceCollection services)
        {
            services.AddScoped<IRabbitMQProducer<EmailDto>, RabbitMQProducer<EmailDto>>();
            services.AddScoped<IRabbitMQConsumer<EmailDto>, RabbitMQConsumer<EmailDto>>();

            return services;
        }
        


        #region Logging

        public static void ConfigureLogging()
        {
            string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")!;

            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile(
                    $"appsettings.{environment}.json", optional: true
                ).Build();
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()                
                .Enrich.WithExceptionDetails(new DestructuringOptionsBuilder()
                    .WithDefaultDestructurers()
                    .WithDestructurers(new[] { new DbUpdateExceptionDestructurer() }))
                .WriteTo.Debug()
                .WriteTo.Console()
                .WriteTo.Elasticsearch(ConfigureElasticSink(configuration, environment))
                .Enrich.WithProperty("Environment", environment)
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }

        private static ElasticsearchSinkOptions ConfigureElasticSink(IConfigurationRoot configuration, string environment)
        {
            return new ElasticsearchSinkOptions(new Uri(configuration["ElastcConfiguration:Uri"]!))
            {
                AutoRegisterTemplate = true,
                IndexFormat = $"{Assembly.GetExecutingAssembly().GetName().Name!.ToLower().Replace(".", "-")}-{environment.ToLower()}-{DateTime.UtcNow:yyyy-MM-dd}",
                NumberOfReplicas = 1,
                NumberOfShards = 2
            };
        }
        #endregion
    }
}
