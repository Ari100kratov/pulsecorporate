﻿using AutoMapper;
using Calendar.API.Application.Abstractions;
using Calendar.API.Application.MeetingService.Commands;
using Calendar.API.Domain.Entities;
using Calendar.API.Domain.EntitiesDto;
using Calendar.API.RabbitMQ.Interfaces;
using MediatR;

namespace Calendar.API.Application.CalendarService.CommandHandlers
{
    public class AddMeetingHandler : IRequestHandler<AddMeetingAsyncCommand, MeetingDto>
    {
        private readonly IMeetingRepository _meetingRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IRabbitEmailProducer<EmailDto> _rabbit;

        public AddMeetingHandler(IMeetingRepository meetingRepository, IUserRepository userRepository,
            IMapper mapper, IRabbitEmailProducer<EmailDto> rabbit)
        {
            _meetingRepository = meetingRepository;
            _userRepository = userRepository;
            _mapper = mapper;
            _rabbit = rabbit;
        }

        public async Task<MeetingDto> Handle(AddMeetingAsyncCommand request, CancellationToken cancellationToken)
        {
            var email = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress")).Value;

            var existUser = await _userRepository.GetByEmailAsync(email, false);

            if (existUser is null)
            {
                var name = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname")).Value;
                var surName = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname")).Value;
                _userRepository.Add(new User { Name = name, Surname = surName, Email = email });
                await _userRepository.SaveChangesAsync(cancellationToken);

                existUser = await _userRepository.GetByEmailAsync(email);
            }

            request.Meeting.CreatorId = existUser!.Id;
            request.Meeting.ParticipantsId = request.Meeting.ParticipantsId!.Except(new Guid[] { existUser!.Id });

            var addMeeting = _mapper.Map<Meeting>(request.Meeting);

            var participants = await _userRepository.GetUsersById(request.Meeting.ParticipantsId!, false);
            addMeeting.Participants = participants;
            addMeeting.Participants.Add(existUser);

            var newMeeting = _meetingRepository.Add(addMeeting);
            await _meetingRepository.SaveChangesAsync(cancellationToken);

            _rabbit.Send(new EmailDto(email, "Add meeting", $"Add meeting topic - {request.Meeting.Topic}, description - {request.Meeting.Description}"));

            newMeeting.Creator = existUser;
            newMeeting.Participants = participants;

            return _mapper.Map<MeetingDto>(newMeeting);
        }
    }
}
