﻿using PulseCorporate.Domain.Abstractions;

namespace PulseCorporate.Domain.Entities;

/// <summary>
/// Represents a role entity.
/// </summary>
public sealed class Role : BaseEntity
{
    /// <summary>
    /// Gets or sets the name of the role.
    /// </summary>
    public required string Name { get; set; }

    /// <summary>
    /// Gets or sets the description of the role.
    /// </summary>
    public string? Description { get; set; }

    ///// <summary>
    ///// Gets or sets the collection of users associated with this role.
    ///// </summary>
    //public ICollection<User> Users { get; set; } = new List<User>();
}
