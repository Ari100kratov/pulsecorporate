
using Post.API;
using Post.API.Infrastructure.ServiceInfrastructure;
using Post.API.Middleware;
using Post.API.RabbitMQ.Consumers;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddServices(builder.Configuration);
builder.Services.AddControllers();


//builder.Services.AddHostedService<RabbitUserConsumer>();


Registrar.ConfigureLogging();
builder.Host.UseSerilog();

builder.Services.AddDistributedMemoryCache();
builder.Services.AddStackExchangeRedisCache(options =>
{
    options.Configuration = builder.Configuration.GetConnectionString("Redis");
});

builder.Services.AddResponseCaching();

var app = builder.Build();

app.UseAuthentication();
app.UseAuthorization();

app.UseLoggingMiddleware();
app.UseExceptionHandlerMiddleware();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.MapMetrics();
app.MapControllers();


app.Services.InitializeInfrastructureServices();

app.Run();