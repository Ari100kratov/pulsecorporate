﻿namespace PulseCorporate.WebUI.Models.Administration
{
    public sealed class MeetingModel
    {
        public required string Topic { get; set; }

        public string? Description { get; set; }

        public required DateTime StartTime { get; set; }

        public required DateTime EndTime { get; set; }

        public IEnumerable<Guid>? ParticipantsId { get; set; }
    }
}
