﻿
using AutoMapper;
using Pulse.Calendar.Domain.EntitiesDto;

namespace Pulse.Calendar.Application.Mapping
{
    public sealed class MeetingProfile : Profile
    {
        public MeetingProfile()
        {
            CreateMap<MeetingDto, Meeting>()
                .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember is not null));
            CreateMap<Meeting, MeetingDto>()
                .ForMember(x => x.ParticipantsId, map => map.Ignore());
        }
    }
}
