﻿using Calendar.API.Domain.Abstractions;

namespace Calendar.API.Domain.Entities
{
    public class Meeting : BaseEntity
    {
        public required string Topic { get; set; }

        public string? Description { get; set; }

        public required DateTime StartTime { get; set; }

        public required DateTime EndTime { get; set; }

        public required Guid CreatorId { get; set; }
        public required User Creator { get; set; }

        public required ICollection<User> Participants { get; set; }
    }
}
