﻿using MediatR;
using PulseCorporate.Domain.EntitiesDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Application.PostService.Commands
{
    public record UpdatePostAsyncCommand(Guid Id, PostDto Post, IEnumerable<Claim> UserClaims) :IRequest<PostDto>;
}
