﻿using Calendar.API.Domain.EntitiesDto;
using MediatR;
using System.Security.Claims;

namespace Calendar.API.Application.MeetingService.Commands
{
    public record UpdateMeetingAsyncCommand(Guid Id, MeetingDto Meeting, IEnumerable<Claim> UserClaims) : IRequest<MeetingDto>;
}
