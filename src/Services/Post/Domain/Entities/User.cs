﻿using Post.API.Domain.Abstractions;

namespace Post.API.Domain.Entities
{
    public sealed class User : BaseEntity
    {
        public required string Name { get; set; }

        public required string Surname { get; set; }

        public required string Email { get; set; }
    }
}
