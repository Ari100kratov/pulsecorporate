﻿namespace Calendar.API.Domain.Abstractions
{
    public abstract class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
