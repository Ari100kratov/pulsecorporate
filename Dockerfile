FROM mcr.microsoft.com/dotnet/sdk:7.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443


FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
COPY /tests /tests
ADD "PulseCorporate.sln" ./
COPY /src /src
RUN dotnet restore "PulseCorporate.sln"
RUN dotnet build "PulseCorporate.sln" -c Release -o ./app/build

FROM build AS publish
RUN dotnet publish "/src/PulseCorporate.WebUI/PulseCorporate.WebUI.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish ./
CMD dotnet dev-certs https --clean; dotnet dev-certs https --trust; dotnet PulseCorporate.WebUI.dll 