﻿using AutoMapper;
using Calendar.API.Domain.Entities;
using Calendar.API.Domain.EntitiesDto;

namespace Calendar.API.Application.Mapping
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDto, User>()
                .ForMember(x => x.Meetings, map => map.Ignore())
                .ForMember(x => x.AddMeetings, map => map.Ignore())
                .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember is not null));
            CreateMap<User, UserDto>();
        }
    }
}
