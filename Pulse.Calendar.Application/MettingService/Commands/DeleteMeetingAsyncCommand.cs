﻿
using MediatR;
using System.Security.Claims;


namespace Pulse.Calendar.Application.MettingService.Commands
{
    public record DeleteMeetingAsyncCommand(Guid Id, IEnumerable<Claim> UserClaims) : IRequest<Guid>;
}
