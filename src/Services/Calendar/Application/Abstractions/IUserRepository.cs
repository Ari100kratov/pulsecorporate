﻿using Calendar.API.Domain.Entities;

namespace Calendar.API.Application.Abstractions
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User?> GetByEmailAsync(string email, bool noTracking = true);

        Task<ICollection<User>> GetUsersById(IEnumerable<Guid> ids, bool noTracking = true);
    }
}
