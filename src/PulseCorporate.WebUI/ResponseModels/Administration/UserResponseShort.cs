﻿using PulseCorporate.Domain.Entities;

namespace PulseCorporate.WebUI.ResponseModels.Administration
{
    internal sealed record UserResponseShort(
        Guid Id,
        string Name,
        string Surname,
        string Email);
}
