﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.IdentityModel.Tokens;
using Post.API.Application.Abstractions;
using Post.API.Application.Mapping;
using Post.API.Application.PostService.CommandHandlers;
using Post.API.Application.PostService.Commands;
using Post.API.Application.PostService.Queries;
using Post.API.Application.PostService.QueriesHandler;
using Post.API.Domain.EntitiesDto;
using Post.API.Infrastructure.Implementation;
using Post.API.Infrastructure.ServiceInfrastructure;
using Post.API.Mapping;
using Post.API.RabbitMQ.Consumers;
using Post.API.RabbitMQ.Interfaces;
using Post.API.RabbitMQ.Producers;
using Serilog;
using Serilog.Exceptions;
using Serilog.Exceptions.Core;
using Serilog.Exceptions.EntityFrameworkCore.Destructurers;
using Serilog.Sinks.Elasticsearch;
using System.Reflection;

namespace Post.API
{
    internal static class Registrar
    {

        internal static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddSingleton((IConfigurationRoot)configuration)
                .AddMediatR(cfg => cfg.RegisterServicesFromAssembly(typeof(Program).Assembly))
                .AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()))
                .AddInfrastructureServices(configuration)
                .InstallHandlers()
                .InstallRepositories()
                .InstallRabbitMQServices();
        }

        private static IServiceCollection InstallHandlers(this IServiceCollection serviceCollection)
        {
            serviceCollection
                //post
                .AddTransient<IRequestHandler<GetPostsAsyncQuery, IEnumerable<PostDto>>, GetPostsHandler>()
                .AddTransient<IRequestHandler<GetPostsForPaginationAsyncQuery, IEnumerable<PostDto>>, GetPostsForPaginationHandler>()
                .AddTransient<IRequestHandler<GetPostByIdAsyncQuery, PostDto>, GetPostByIdHandler>()
                .AddTransient<IRequestHandler<AddPostAsyncCommand, PostDto>, AddPostHandler>()
                .AddTransient<IRequestHandler<UpdatePostAsyncCommand, PostDto>, UpdatePostHandler>()
                .AddTransient<IRequestHandler<DeletePostAsyncCommand, Guid>, DeletePostHandler>();

            return serviceCollection;
        }

        private static IServiceCollection InstallRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddTransient<IPostRepository, PostRepository>()
                .AddTransient<IUserRepository, UserRepository>();
            return serviceCollection;
        }

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                //post
                cfg.AddProfile<PostProfile>();
                cfg.AddProfile<PostUiProfile>();
                //user
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<UserUiProfile>();

            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }
        private static IServiceCollection InstallRabbitMQServices(this IServiceCollection services)
        {
            services.AddScoped<IRabbitEmailProducer<EmailDto>, RabbitEmailProducer<EmailDto>>();
            //services.AddScoped<BackgroundService, RabbitUserConsumer>();

            return services;
        }

        #region Auth

        private static IServiceCollection AddAuth(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            const string authPolicyName = "pulse-auth-policy";
            string issuingAuthority = configuration.GetSection("Keycloak").Value!;
            const string validAudience = "account";

            serviceCollection.AddAuthentication().AddJwtBearer(jwtBearerOptions =>
            {
                jwtBearerOptions.Authority = issuingAuthority;
                jwtBearerOptions.RequireHttpsMetadata = false;
                jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = issuingAuthority,
                    ValidAudience = validAudience,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true
                };
            });

            serviceCollection.AddAuthorization(authorizationOptions =>
            {
                authorizationOptions.AddPolicy(authPolicyName, policy =>
                {
                    policy.AuthenticationSchemes.Add(JwtBearerDefaults.AuthenticationScheme);
                    policy.RequireAuthenticatedUser();
                });
            });

            serviceCollection.AddControllers(mvcOptions =>
            {
                mvcOptions.Filters.Add(new AuthorizeFilter(authPolicyName));
            });

            return serviceCollection;
        }

        #endregion


        #region Logging

        public static void ConfigureLogging()
        {
            string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")!;

            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile(
                    $"appsettings.{environment}.json", optional: true
                ).Build();
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .Enrich.WithExceptionDetails(new DestructuringOptionsBuilder()
                    .WithDefaultDestructurers()
                    .WithDestructurers(new[] { new DbUpdateExceptionDestructurer() }))
                .WriteTo.Debug()
                .WriteTo.Console()
                .WriteTo.Elasticsearch(ConfigureElasticSink(configuration, environment))
                .Enrich.WithProperty("Environment", environment)
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }

        private static ElasticsearchSinkOptions ConfigureElasticSink(IConfigurationRoot configuration, string environment)
        {
            return new ElasticsearchSinkOptions(new Uri(configuration["ElastcConfiguration:Uri"]!))
            {
                AutoRegisterTemplate = true,
                IndexFormat = $"{Assembly.GetExecutingAssembly().GetName().Name!.ToLower().Replace(".", "-")}-{environment.ToLower()}-{DateTime.UtcNow:yyyy-MM-dd}",
                NumberOfReplicas = 1,
                NumberOfShards = 2
            };
        }
        #endregion
    }

}
