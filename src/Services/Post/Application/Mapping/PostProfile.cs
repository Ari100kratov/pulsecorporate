﻿using AutoMapper;
using Post.API.Domain.Entities;
using Post.API.Domain.EntitiesDto;

namespace Post.API.Application.Mapping
{
    public sealed class PostProfile : Profile
    {
        public PostProfile()
        {
            CreateMap<PostDto, Posts>()
                .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember is not null));
            CreateMap<Posts, PostDto>();
        }
    }
}
