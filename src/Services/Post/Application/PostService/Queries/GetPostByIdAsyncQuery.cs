﻿using MediatR;
using Post.API.Domain.EntitiesDto;

namespace Post.API.Application.PostService.Queries
{
    public record GetPostByIdAsyncQuery(Guid Id) : IRequest<PostDto>;
}
