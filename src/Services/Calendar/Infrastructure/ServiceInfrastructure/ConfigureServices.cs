﻿using Calendar.API.Infrastructure.Interfaces;
using Calendar.API.Infrastructure.ServiceInfrastructure.Persistance;
using Microsoft.EntityFrameworkCore;
using Npgsql;

namespace Calendar.API.Infrastructure.ServiceInfrastructure
{
    public static class ConfigureServices
    {
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
        {
            var portString = configuration["PostgresPort"];
            portString = string.IsNullOrEmpty(portString) ? "5432" : portString;
            int port = int.Parse(portString);

            var conStrBuilder = new NpgsqlConnectionStringBuilder(configuration.GetConnectionString("CalendarContext"))
            {
                Password = configuration["PostgresPassword"],
                Host = configuration["PostgresHost"],
                Port = port,
                Username = configuration["PostgresUsername"],
                Database = configuration["PostgresDatabase"]
            };

            var calendarContext = conStrBuilder.ConnectionString;
            services.AddDbContext<CalendarContext>(options => options.UseNpgsql(calendarContext));

            services.AddTransient<IApplicationContext>(provider => provider.GetRequiredService<CalendarContext>());

            return services;
        }


        public static async void InitializeInfrastructureServices(this IServiceProvider provider)
        {
            using var scope = provider.CreateScope();
            var dbContext = scope.ServiceProvider.GetRequiredService<CalendarContext>();
            dbContext.Database.EnsureCreated();

            //await dbContext.Database.MigrateAsync();
        }
    }
}
