# PulseCorporate

https://gitlab.com/Ari100kratov/pulsecorporate/-/wikis/Корпоративный-портал

## Инструменты

Для работы с зависимостями frontend-приложения использовать **yarn** https://classic.yarnpkg.com/lang/en/docs/getting-started/

## Создание новой миграции
```
dotnet ef migrations add InitialCreate --context PulseCorporateContext --output-dir Migrations --project src/Infrastructure/PulseCorporate.Infrastructure.PostgreSql --startup-project src/PulseCorporate.WebUI
```
