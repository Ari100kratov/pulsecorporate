﻿using AutoFixture;
using AutoFixture.AutoMoq;
using AutoMapper;
using Moq;
using Newtonsoft.Json;
using PulseCorporate.Application.PostService.QueriesHandlers;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions;
using PulseCorporate.Repositories.Abstractions.Administration;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace tests.PulseCorporate.IntegrationTests.PostService.QueriesHandlers
{
    public class GetPostsHandlerTests:IClassFixture<QueriesRoleFixture>
    {
        private readonly HttpClient _httpClient;
        private readonly string _baseUri;
        private readonly Fixture _autoFixture = new Fixture();
        public GetPostsHandlerTests(QueriesRoleFixture roleFixture)
        {
            _httpClient = new HttpClient();
            var configuretion = roleFixture.Configuration;
            _baseUri = configuretion["BaseUri"];
        }

        [Fact]
        public async Task PostsShouldBeGotSuccessfully()
        {
            //Arrange
            string responseUrl = $"{_baseUri}/api/posts";
            var postGuids = new List<Guid>
            {
                Guid.Parse("bf89b965-3efb-40f5-ace3-895f86aaf1c7"),
                Guid.Parse("c7a490b3-b458-4560-9c15-ca557a234cdb"),
                Guid.Parse("e9e9c58e-c7b0-4c9b-bd1d-845b318032a9"),
                Guid.Parse("90189aa0-3086-4d5b-8283-79dc90b91c95")
            };

            //Act
            var getPostsResponse = await _httpClient.GetAsync(responseUrl);

            //Assert
            getPostsResponse.IsSuccessStatusCode.ShouldBeTrue();
            getPostsResponse.StatusCode.ShouldBe(HttpStatusCode.OK);
            var postsDto = JsonConvert.DeserializeObject<List<PostDto>>(await getPostsResponse.Content.ReadAsStringAsync());
            postsDto.ShouldNotBeNull();
            Assert.Collection(postGuids,
                e =>
                {
                    Assert.Equal(e, postsDto[1].Id);
                },
                e =>
                {
                    Assert.Equal(e, postsDto[2].Id);
                },
                e =>
                {
                    Assert.Equal(e, postsDto[3].Id);
                },
                e =>
                {
                    Assert.Equal(e, postsDto[0].Id);
                }
                );
        }
    }
}
