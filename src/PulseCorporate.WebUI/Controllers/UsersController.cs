﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AutoMapper;
using MediatR;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using PulseCorporate.Application.UserService.Queries;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.WebUI.Models.Administration;
using PulseCorporate.WebUI.ResponseModels.Administration;
using PulseCorporate.Application.UserService.Commands;
using PulseCorporate.Application.RoleService.Queries;

namespace PulseCorporate.WebUI.Controllers
{
    /// <summary>
    /// API controller for managing users.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private readonly IMapper _mapper;
        private readonly ISender _sender;

        /// <summary>
        /// Initializes a new instance of the UsersController class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="sender">The sender for MediatR requests.</param>
        public UsersController(ILogger<UsersController> logger, IMapper mapper, ISender sender)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger), "Uninitialized property");
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper), "Uninitialized property");
            _sender = sender ?? throw new ArgumentNullException(nameof(sender), "Uninitialized property");
        }

        /// <summary>
        /// Retrieves a user.
        /// </summary>        
        /// <returns>The user.</returns>
        [HttpGet("CurrentUser", Name = "GetUser")]
        [ProducesResponseType(typeof(UserResponseShort), 200)]
        public async Task<ActionResult> GetUser()
        {
            var user = await _sender.Send(new GetUserByEmailAsyncQuery(HttpContext.User.Claims));

            return Ok(_mapper.Map<UserResponseShort>(user));
        }

        /// <summary>
        /// Retrieves a list of users.
        /// </summary>
        /// <returns>A list of users.</returns>
        [HttpGet("AllUsers")]
        [ProducesResponseType(typeof(List<UserResponseShort>), 200)]
        public async Task<IActionResult> GetUsers()
        {
            return Ok(_mapper.Map<List<UserResponseShort>>(await _sender.Send(new GetUsersAsyncQuery())));
        }

        /// <summary>
        /// Updates an existing user.
        /// </summary>        
        /// <param name="userModel">The updated user details.</param>
        /// <returns>The updated user.</returns>
        [HttpPut("SetUser")]
        [ProducesResponseType(typeof(UserResponseShort), 202)]
        public async Task<ActionResult> UpdateUser([FromBody] UserModel userModel)
        {
            var updatedUser = await _sender.Send(new UpdateUserAsyncCommand(_mapper.Map<UserDto>(userModel), HttpContext.User.Claims));

            return AcceptedAtRoute(nameof(GetUser), new { id = updatedUser.Id }, _mapper.Map<UserResponseShort>(updatedUser));
        }
    }
}