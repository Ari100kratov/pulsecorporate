﻿using AutoMapper;
using Calendar.API.Application.MeetingService.Commands;
using Calendar.API.Application.MeetingService.Queries;
using Calendar.API.Domain.EntitiesDto;
using Calendar.API.Models;
using Calendar.API.Models.ResponceModels;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Calendar.API.Controllers
{
    /// <summary>
    /// API controller for managing meetings.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class MeetingsController : ControllerBase
    {
        private readonly ILogger<MeetingsController> _logger;
        private readonly IMapper _mapper;
        private readonly ISender _sender;

        /// <summary>
        /// Initializes a new instance of the MeetingsController class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="sender">The sender for MediatR requests.</param>
        public MeetingsController(ILogger<MeetingsController> logger, IMapper mapper, ISender sender)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger), "Uninitialized property");
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper), "Uninitialized property");
            _sender = sender ?? throw new ArgumentNullException(nameof(sender), "Uninitialized property");
        }

        /// <summary>
        /// Retrieves a meeting.
        /// </summary>        
        /// <returns>The meeting.</returns>
        [HttpGet(Name = "GetMeetings")]
        [ProducesResponseType(typeof(IEnumerable<MeetingResponse>), 200)]
        public async Task<ActionResult> GetMeeting()
        {
            var meeting = await _sender.Send(new GetMeetingsOfUserAsyncQuery(HttpContext.User.Claims));

            return Ok(_mapper.Map<IEnumerable<MeetingResponse>>(meeting));
        }

        /// <summary>
        /// Adds a new meeting.
        /// </summary>
        /// <param name="meetingModel">The meeting details to add.</param>
        /// <returns>The newly added meeting.</returns>
        [HttpPost]
        [ProducesResponseType(typeof(MeetingResponse), 201)]
        public async Task<ActionResult> AddPost([FromBody] MeetingModel meetingModel)
        {
            var addedMeeting = await _sender.Send(new AddMeetingAsyncCommand(_mapper.Map<MeetingDto>(meetingModel), HttpContext.User.Claims));

            return CreatedAtRoute("GetMeetings", new { id = addedMeeting.Id }, _mapper.Map<MeetingResponse>(addedMeeting));
        }

        /// <summary>
        /// Updates an existing meeting.
        /// </summary>        
        /// <param name="meetingModel">The updated meeting details.</param>
        /// <returns>The updated meeting.</returns>
        [HttpPut("{id:Guid}")]
        [ProducesResponseType(typeof(MeetingResponse), 202)]
        public async Task<ActionResult> UpdateMeeting(Guid id, [FromBody] MeetingModel meetingModel)
        {
            var updatedMeeting = await _sender.Send(new UpdateMeetingAsyncCommand(id, _mapper.Map<MeetingDto>(meetingModel), HttpContext.User.Claims));

            return AcceptedAtRoute("GetMeetings", new { id = updatedMeeting.Id }, _mapper.Map<MeetingResponse>(updatedMeeting));
        }

        /// <summary>
        /// Deletes a meeting by its unique identifier.
        /// </summary>
        /// <param name="id">The unique identifier of the meeting to delete.</param>
        /// <returns>A message confirming the deletion.</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(string), 200)]
        public async Task<IActionResult> DeleteMeeting(Guid id)
        {
            await _sender.Send(new DeleteMeetingAsyncCommand(id, HttpContext.User.Claims));

            return Ok($"Post with id = {id} has been removed");
        }
    }
}
