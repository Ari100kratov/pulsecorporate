﻿using AutoMapper;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.WebUI.Models.Administration;
using PulseCorporate.WebUI.ResponseModels.Administration;

namespace PulseCorporate.WebUI.Mapping
{
    public class PostUiProfile: Profile
    {
        public PostUiProfile()
        {
            CreateMap<PostDto, PostResponse>();
            CreateMap<PostDto, PostResponseShort>();
            CreateMap<PostModel, PostDto>()
                .ForMember(x => x.Id, map => map.Ignore())
                .ForMember(x => x.Author, map => map.Ignore())
                .ForMember(x => x.AuthorId, map => map.Ignore());
        }
    }
}
