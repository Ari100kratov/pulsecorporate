﻿using MediatR;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Application.RoleService.Queries
{
    public record GetRoleByIdAsyncQuery(Guid Id) : IRequest<RoleDto>;
}
