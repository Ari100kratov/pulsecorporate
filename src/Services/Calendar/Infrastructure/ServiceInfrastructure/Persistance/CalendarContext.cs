﻿using Calendar.API.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Calendar.API.Infrastructure.ServiceInfrastructure.Persistance
{
    public class CalendarContext: DbContext, IApplicationContext
    {
        public CalendarContext(DbContextOptions<CalendarContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<Role>().HasData(SeedData.Roles);
            //modelBuilder.Entity<Post>();
            //modelBuilder.Entity<Meeting>();
            //modelBuilder.Entity<User>();
        }

    }
}
