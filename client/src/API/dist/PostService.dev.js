"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getPosts = exports.axiosInstance = void 0;

var _axios = _interopRequireDefault(require("axios"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// import axios from "axios";
// export const getPosts = async (page, limit) => {
// 	try {
// 		const response = await axios.get(`${process.env.REACT_APP_BACKEND_API_URL}/Posts?page=${page}&limit=${limit}`);
// 		return response.data;
// 	} catch (error) {
// 		console.error('There was an error!', error);
// 		return [];
// 	}
// };
// PostService.js
var axiosInstance = _axios["default"].create({
  baseURL: process.env.REACT_APP_BACKEND_API_URL
});

exports.axiosInstance = axiosInstance;

var getPosts = function getPosts(page, limit) {
  var response;
  return regeneratorRuntime.async(function getPosts$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return regeneratorRuntime.awrap(axiosInstance.get("/Posts?page=".concat(page, "&limit=").concat(limit)));

        case 3:
          response = _context.sent;
          return _context.abrupt("return", response.data);

        case 7:
          _context.prev = 7;
          _context.t0 = _context["catch"](0);
          console.error('There was an error!', _context.t0);
          return _context.abrupt("return", []);

        case 11:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[0, 7]]);
};

exports.getPosts = getPosts;