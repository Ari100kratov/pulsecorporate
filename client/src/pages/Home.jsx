import React from "react";

import { useKeycloak } from "@react-keycloak/web";
import WelcomePage from "../Components/WelcomePage";
import PostsPage from "./Posts";

function Home() {
  const { keycloak, initialized } = useKeycloak();

  return (
    <>
      {!keycloak.authenticated && <WelcomePage />}
      {!!keycloak.authenticated && (
        <>
          <PostsPage />
        </>
      )}
    </>
  );
}

export default Home;
