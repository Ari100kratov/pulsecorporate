﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using PulseCorporate.Application.PostService.Queries;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace PulseCorporate.Application.PostService.QueriesHandlers
{
    public class GetPostByIdHandler:IRequestHandler<GetPostByIdAsyncQuery, PostDto>
    {
        private readonly IPostRepository _postRepository;
        private readonly IMapper _mapper;
        private readonly IDistributedCache _distributedCache;

        public GetPostByIdHandler(IPostRepository postRepository, IMapper mapper, IDistributedCache distributedCache)
        {
            _postRepository = postRepository;
            _mapper = mapper;
            _distributedCache = distributedCache;
        }

        public async Task<PostDto> Handle(GetPostByIdAsyncQuery request, CancellationToken cancellationToken)
        {
            string serializedPost = await _distributedCache.GetStringAsync($"posts:{request.Id}");
            if (serializedPost != null)
            {
                var deserializedPost = JsonSerializer.Deserialize<Post>(serializedPost);
                return _mapper.Map<PostDto>(deserializedPost);
            }

            var existPost = await _postRepository.GetByIdAsync(request.Id)??
                throw new KeyNotFoundException($"Not found {nameof(Post)} with this id {request.Id}");

            if (existPost != null)
            {
                await _distributedCache.SetStringAsync(
                   key: $"posts:{request.Id}",
                   value: JsonSerializer.Serialize(value: existPost),
                   options: new DistributedCacheEntryOptions
                   {
                       SlidingExpiration = TimeSpan.FromSeconds(10),
                   });
            }

            return _mapper.Map<PostDto>(existPost);
        }
    }
}
