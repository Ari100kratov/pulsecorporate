﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Post.API.Domain.Entities;

namespace Post.API.Infrastructure.ServiceInfrastructure.Persistance.Configurations
{
    internal class PostConfiguration : IEntityTypeConfiguration<Posts>
    {
        public void Configure(EntityTypeBuilder<Posts> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(x => x.Name)
                .HasMaxLength(50);
        }
    }
}
