﻿namespace User.API.Models
{
    /// <summary>
    /// Internal model for representing a user.
    /// </summary>
    public sealed class UserModel
    {
        public required Guid Id { get; set; }
        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        public required string Name { get; set; }

        /// <summary>
        /// Gets or sets the surname of the user.
        /// </summary>      
        public required string Surname { get; set; }

        public required string Email { get; set; }

    }
}
