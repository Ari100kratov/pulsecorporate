﻿using AutoMapper;
using Calendar.API.Application.Abstractions;
using Calendar.API.Application.CalendarService.CommandHandlers;
using Calendar.API.Application.Mapping;
using Calendar.API.Application.MeetingService.Commands;
using Calendar.API.Application.MeetingService.Queries;
using Calendar.API.Application.MeetingService.QueriesHandlers;
using Calendar.API.Domain.EntitiesDto;
using Calendar.API.Infrastructure.Implementation;
using Calendar.API.Infrastructure.ServiceInfrastructure;
using Calendar.API.Mapping;
using Calendar.API.RabbitMQ.Consumers;
using Calendar.API.RabbitMQ.Interfaces;
using Calendar.API.RabbitMQ.Producers;
using MediatR;
using Serilog;
using Serilog.Exceptions;
using Serilog.Exceptions.Core;
using Serilog.Exceptions.EntityFrameworkCore.Destructurers;
using Serilog.Sinks.Elasticsearch;
using System.Reflection;

namespace Calendar.API
{
    public static class Registrar
    {
        internal static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddSingleton((IConfigurationRoot)configuration)
                .AddMediatR(cfg => cfg.RegisterServicesFromAssembly(typeof(Program).Assembly))
                .AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()))
                .AddInfrastructureServices(configuration)
                .InstallHandlers()
                .InstallRepositories()
                .InstallRabbitServices();
        }

        private static IServiceCollection InstallHandlers(this IServiceCollection serviceCollection)
        {
            serviceCollection
                //meeting
                .AddTransient<IRequestHandler<GetMeetingsOfUserAsyncQuery, IEnumerable<MeetingDto>>, GetMeetingsOfUserHandler>()
                .AddTransient<IRequestHandler<AddMeetingAsyncCommand, MeetingDto>, AddMeetingHandler>()
                .AddTransient<IRequestHandler<UpdateMeetingAsyncCommand, MeetingDto>, UpdateMeetingHandler>()
                .AddTransient<IRequestHandler<DeleteMeetingAsyncCommand, Guid>, DeleteMeetingHandler>();
            return serviceCollection;
        }

        private static IServiceCollection InstallRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddTransient<IUserRepository, UserRepository>()
                .AddTransient<IMeetingRepository, MeetingRepository>();
            return serviceCollection;
        }

        #region Auth

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                //user
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<UserUiProfile>();
                //meeting
                cfg.AddProfile<MeetingProfile>();
                cfg.AddProfile<MeetingUiProfile>();
            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }
        private static IServiceCollection InstallRabbitServices(this IServiceCollection services)
        {
            services.AddScoped<IRabbitEmailProducer<EmailDto>, RabbitEmailProducer<EmailDto>>();
            services.AddScoped<BackgroundService,RabbitUserConsumer> ();
            return services;
        }
        #endregion


        #region Logging

        public static void ConfigureLogging()
        {
            string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")!;

            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile(
                    $"appsettings.{environment}.json", optional: true
                ).Build();
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .Enrich.WithExceptionDetails(new DestructuringOptionsBuilder()
                    .WithDefaultDestructurers()
                    .WithDestructurers(new[] { new DbUpdateExceptionDestructurer() }))
                .WriteTo.Debug()
                .WriteTo.Console()
                .WriteTo.Elasticsearch(ConfigureElasticSink(configuration, environment))
                .Enrich.WithProperty("Environment", environment)
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }

        private static ElasticsearchSinkOptions ConfigureElasticSink(IConfigurationRoot configuration, string environment)
        {
            return new ElasticsearchSinkOptions(new Uri(configuration["ElastcConfiguration:Uri"]!))
            {
                AutoRegisterTemplate = true,
                IndexFormat = $"{Assembly.GetExecutingAssembly().GetName().Name!.ToLower().Replace(".", "-")}-{environment.ToLower()}-{DateTime.UtcNow:yyyy-MM-dd}",
                NumberOfReplicas = 1,
                NumberOfShards = 2
            };
        }
        #endregion
    }
}
