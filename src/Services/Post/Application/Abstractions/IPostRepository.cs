﻿using Post.API.Domain.Entities;

namespace Post.API.Application.Abstractions
{
    /// <summary>
    /// Repositiry interface to manage Post entities.
    /// </summary>
    public interface IPostRepository : IRepository<Posts>
    {
        Task<IEnumerable<Posts>> GetForPaginationAsync(int page, int quantity);
    }
}
