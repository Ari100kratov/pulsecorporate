﻿using AutoMapper;
using MediatR;
using PulseCorporate.Application.PostService.Commands;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions.Administration;
using RabbitMQService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Application.PostService.CommandHandlers
{
    public class UpdatePostHandler:IRequestHandler<UpdatePostAsyncCommand, PostDto>
    {
        private readonly IUserRepository _userRepository;
        private readonly IPostRepository _postRepository;
        private readonly IMapper _mapper;
        private readonly IRabbitMQProducer<EmailDto> _rabbit;

        public UpdatePostHandler(IPostRepository postRepository, IUserRepository userRepository, IMapper mapper, IRabbitMQProducer<EmailDto> rabbit)
        {
            _postRepository = postRepository;
            _userRepository = userRepository;
            _mapper = mapper;
            _rabbit = rabbit;
        }

        public async Task<PostDto> Handle(UpdatePostAsyncCommand request, CancellationToken cancellationToken)
        {
            var email = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress")).Value;

            var existUser = await _userRepository.GetByEmailAsync(email);

            if (existUser is null)
            {
                var name = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname")).Value;
                var surName = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname")).Value;
                _userRepository.Add(new User { Name = name, Surname = surName, Email = email });
                await _userRepository.SaveChangesAsync(cancellationToken);

                existUser = await _userRepository.GetByEmailAsync(email);
            }            

            request.Post.AuthorId = existUser!.Id;
            request.Post.Author = _mapper.Map<UserDto>(existUser);
            request.Post.Id = request.Id;

            var existPost = await _postRepository.GetByIdAsync(request.Id) ??
               throw new KeyNotFoundException($"Not found {nameof(Post)} with this id: {request.Id}");
            _mapper.Map(request.Post, existPost);

            _postRepository.Update(_mapper.Map<Post>(existPost));
            await _postRepository.SaveChangesAsync(cancellationToken);

            _rabbit.SendMessage(new EmailDto(email, "Update post", $"Update post new name - {request.Post.Name}, new description - {request.Post.Description}"));

            return request.Post;
        }
    }
    
}
