﻿using AutoMapper;
using Pulse.User.Domain.EntitiesDto;
using User.API.Models;
using User.API.ResponseModels;

namespace User.API.Mapping
{
    public sealed class UserUiProfile : Profile
    {
        public UserUiProfile()
        {
            CreateMap<UserDto, UserResponseShort>();
            CreateMap<UserModel, UserDto>();
               
        }
    }
}
