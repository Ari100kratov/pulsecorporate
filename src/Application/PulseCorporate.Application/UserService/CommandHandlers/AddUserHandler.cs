﻿

using AutoMapper;
using MediatR;
using PulseCorporate.Application.UserService.Commands;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions.Administration;
using RabbitMQService.Interfaces;

namespace PulseCorporate.Application.UserService.CommandHandlers
{
    public class AddUserHandler : IRequestHandler<AddUserAsyncCommand, UserDto>
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IRabbitMQProducer<EmailDto> _rabbit;

        public AddUserHandler(IUserRepository userRepository, IMapper mapper, IRabbitMQProducer<EmailDto> rabbit)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _rabbit = rabbit;
        }

        public async Task<UserDto> Handle(AddUserAsyncCommand request, CancellationToken cancellationToken)
        {
            _userRepository.Add(_mapper.Map<User>(request.User));

            await _userRepository.SaveChangesAsync(cancellationToken);
       
            _rabbit.SendMessage(new EmailDto("ilyagur19999@yandex.ru", "Add user", "User"));

            return request.User;
        }
    }
}
