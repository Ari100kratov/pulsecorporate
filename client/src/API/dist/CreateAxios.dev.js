"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createAxiosInstance = void 0;

var _axios = _interopRequireDefault(require("axios"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var createAxiosInstance = function createAxiosInstance(keycloak) {
  if (!keycloak || !keycloak.token) {
    throw new Error('Keycloak instance and token are required to create Axios instance');
  }

  var axiosInstance = _axios["default"].create({
    baseURL: process.env.REACT_APP_BACKEND_API_URL,
    headers: {
      Authorization: "Bearer ".concat(keycloak.token)
    }
  });

  var setAxiosAuthHeader = function setAxiosAuthHeader() {
    if (keycloak.isTokenExpired(5)) {
      keycloak.updateToken(30).then(function (refreshed) {
        if (refreshed) {
          console.log('Token was successfully refreshed');
          axiosInstance.defaults.headers['Authorization'] = "Bearer ".concat(keycloak.token);
        }
      })["catch"](function (error) {
        console.error('Failed to refresh token', error);
      });
    }
  };

  setAxiosAuthHeader();
  var intervalId = setInterval(setAxiosAuthHeader, 60000);

  axiosInstance.cancelTokenRefresh = function () {
    return clearInterval(intervalId);
  };

  return axiosInstance;
};

exports.createAxiosInstance = createAxiosInstance;