﻿using MediatR;
using Pulse.Calendar.Domain.EntitiesDto;
using System.Security.Claims;


namespace Pulse.Calendar.Application.MettingService.Commands
{
    public record AddMeetingAsyncCommand(MeetingDto Meeting, IEnumerable<Claim> UserClaims) : IRequest<MeetingDto>;
}
