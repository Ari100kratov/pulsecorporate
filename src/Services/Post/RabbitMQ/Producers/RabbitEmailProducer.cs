﻿using System.Text.Json;
using System.Text;
using Post.API.RabbitMQ.Interfaces;
using RabbitMQ.Client;

namespace Post.API.RabbitMQ.Producers
{
    public class RabbitEmailProducer<T> : IRabbitEmailProducer<T>, IDisposable
    {
        private readonly IConnection _connection;
        private readonly ConnectionFactory _connectionFactory;
        private readonly IModel _model;
        private readonly IConfiguration _configuration;

        public RabbitEmailProducer()
        {
            _configuration = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();
            _connectionFactory = new ConnectionFactory() { HostName = _configuration["RabbitMQServices:HostName"], UserName = _configuration["RabbitMQServices:UserName"], Password = _configuration["RabbitMQServices:Password"] };
            _connection = _connectionFactory.CreateConnection();
            _model = _connection.CreateModel();
            _model.QueueDeclare(queue: _configuration["RabbitMQServices:QueueName"],
                               durable: false,
                               exclusive: false,
                               autoDelete: false,
                               arguments: null);
        }

        public void Send(T objectDto)
        {
            var jsonBody = JsonSerializer.Serialize(objectDto);
            var utf8Body = Encoding.UTF8.GetBytes(jsonBody);

            _model.BasicPublish(exchange: "",
                           routingKey: _configuration["RabbitMQServices:QueueNameEmail"],
                           basicProperties: null,
                           body: utf8Body);
        }

        public void Dispose()
        {
            _model.Close();
            _connection.Close();
        }
    }
}
