﻿using Pulse.Calendar.Application.Interfaces;
using Pulse.Calendar.Domain.Entities;
using Pulse.Calendar.Repositories.Abstractions;
using Pulse.Calendar.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulse.Calendar.Repositories.Implementation
{
    /// <summary>
    /// Repository for managing meeting entities.
    /// </summary>
    public sealed class MeetingRepository : BaseRepository<Meeting>, IMeetingRepository
    {

        /// <summary>
        /// Initializes a new instance of the MeetingRepository class.
        /// </summary>
        /// <param name="context">The application context.</param>
        public MeetingRepository(IApplicationContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Meeting>> GetMeetingsOfUserAsync(User user)
        {
            return await _entitySet.AsNoTracking().Where(x => x.Participants.Contains(user))
                .Include(c => c.Creator).Include(p => p.Participants).ToListAsync();
        }

        public override async Task<Meeting?> GetByIdAsync(Guid id, bool noTracking = true)
        {
            return noTracking ? await _entitySet.AsNoTracking().Include(p => p.Participants).FirstOrDefaultAsync(x => x.Id == id) :
                await _entitySet.Include(p => p.Participants).FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
