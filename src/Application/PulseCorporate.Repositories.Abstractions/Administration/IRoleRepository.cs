﻿using PulseCorporate.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Repositories.Abstractions.Administration
{

    public interface IRoleRepository : IRepository<Role>
    {

    }
}
