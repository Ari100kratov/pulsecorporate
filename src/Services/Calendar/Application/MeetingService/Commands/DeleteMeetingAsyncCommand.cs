﻿using MediatR;
using System.Security.Claims;

namespace Calendar.API.Application.MeetingService.Commands
{
    public record DeleteMeetingAsyncCommand(Guid Id, IEnumerable<Claim> UserClaims) : IRequest<Guid>;
}
