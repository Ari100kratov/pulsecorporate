import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import IconButton from "@mui/material/IconButton";
import { Box, Card, CardContent, Typography } from "@mui/material";
export const Post = ({
  name,
  description,
  createdDate,
  author,
  onEdit,
  onDelete,
}) => (
  <Card sx={{ mb: 2, boxShadow: 3 }}>
    <CardContent>
      <Typography variant="h6">{name}</Typography>
      <Typography color="textSecondary">{description}</Typography>
      <Typography variant="body2">
        Дата: {new Date(createdDate).toLocaleDateString()}
      </Typography>
      <Typography variant="body2">
        Автор: {author.name} {author.surname}
      </Typography>
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        mt={2}
      >
        <IconButton color="primary" onClick={() => onEdit()}>
          <EditIcon />
        </IconButton>
        <IconButton color="secondary" onClick={() => onDelete()}>
          <DeleteIcon />
        </IconButton>
      </Box>
    </CardContent>
  </Card>
);
