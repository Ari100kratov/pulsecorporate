﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailNotification.DAL.Models
{
    internal class EmailMessage
    {
        public string Email {  get; set; }
        public string Message { get; set; }
        public string Subjct { get; set; }
        public DateTime Date { get; set; }
    }
}
