﻿using Calendar.API.Domain.Abstractions;

namespace Calendar.API.Domain.EntitiesDto
{
    public class UserDto:BaseEntityDto
    {
        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        public required string Name { get; set; }

        /// <summary>
        /// Gets or sets the surname of the user.
        /// </summary>      
        public required string Surname { get; set; }

        /// <summary>
        /// Gets or sets the email of the user.
        /// </summary> 
        public required string Email { get; set; }
    }
}
