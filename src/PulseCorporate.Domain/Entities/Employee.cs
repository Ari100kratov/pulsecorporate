﻿using PulseCorporate.Domain.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Domain.Entities
{
    public sealed class Employee:BaseEntity
    {
        public required string FirstName { get; set; }
        public required string LastName { get; set;}
        private User? _user;

        public User User 
        {
            set => _user = value;
            get => _user ?? throw new InvalidOperationException("Uninitialized property: " + nameof(User));
        }



    }
}
