﻿namespace Post.API.RabbitMQ.Interfaces
{
    public interface IRabbitUserConsumer
    {
        public void Execute();
    }
}
