﻿using Pulse.Calendar.Domain.Abstractions;

namespace Pulse.Calendar.Domain.Entities
{
    public sealed class User : BaseEntity
    {
        public required string Name { get; set; }

        public required string Surname { get; set; }

        public required string Email { get; set; }

        public ICollection<Meeting>? AddMeetings { get; set; }
        public ICollection<Meeting>? Meetings { get; set; }
    }
}
