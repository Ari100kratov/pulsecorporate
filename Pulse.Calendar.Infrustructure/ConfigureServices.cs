﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Npgsql;
using Pulse.Calendar.Application.Interfaces;
using Pulse.Calendar.Infrustructure.Pesrsistence;

namespace Pulse.Calendar.Infrustructure
{
    public static class ConfigureServices
    {
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
        {
            var portString = configuration["PostgresPort"];
            portString = string.IsNullOrEmpty(portString) ? "5432" : portString;
            int port = int.Parse(portString);

            var conStrBuilder = new NpgsqlConnectionStringBuilder(configuration.GetConnectionString("PulseCalendarContext"))
            {
                Password = "postgres",
                Host = "62.84.121.32",
                Port = 5432,
                Username = "postgres",
                Database = "PulseCorporateDB"
            };


            var pulseCalendarContext = conStrBuilder.ConnectionString;
            services.AddDbContext<PulseCalendarContext>(options => options.UseNpgsql(pulseCalendarContext
                ));

            services.AddTransient<IApplicationContext>(provider => provider.GetRequiredService<PulseCalendarContext>());

            return services;
        }


        public static async void InitializeInfrastructureServices(this IServiceProvider provider)
        {
            using var scope = provider.CreateScope();
            var dbContext = scope.ServiceProvider.GetRequiredService<PulseCalendarContext>();
            //dbContext.Database.EnsureDeleted();
            dbContext.Database.EnsureCreated();
        }
    }
}
