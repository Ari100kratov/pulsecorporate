﻿using Calendar.API.Domain.EntitiesDto;
using MediatR;
using System.Security.Claims;

namespace Calendar.API.Application.MeetingService.Queries
{
    public sealed record GetMeetingsOfUserAsyncQuery(IEnumerable<Claim> UserClaims) : IRequest<IEnumerable<MeetingDto>>;
}
