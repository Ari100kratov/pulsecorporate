﻿using MediatR;
using Post.API.Domain.EntitiesDto;

namespace Post.API.Application.PostService.Queries
{
    public record GetPostsAsyncQuery() : IRequest<IEnumerable<PostDto>>;
}
