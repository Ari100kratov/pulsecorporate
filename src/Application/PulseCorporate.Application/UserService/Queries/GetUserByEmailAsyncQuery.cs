﻿using MediatR;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Application.UserService.Queries
{
    public record GetUserByEmailAsyncQuery(IEnumerable<Claim> UserClaims) : IRequest<UserDto>;
}
