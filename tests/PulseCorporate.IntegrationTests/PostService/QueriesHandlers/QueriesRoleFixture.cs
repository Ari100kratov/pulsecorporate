﻿using Microsoft.Extensions.Configuration;


namespace tests.PulseCorporate.IntegrationTests.PostService.QueriesHandlers
{
    public class QueriesRoleFixture: IDisposable
    {
        public IConfiguration Configuration;
        public QueriesRoleFixture()
        {
            string rootPath = new DirectoryInfo(Environment.CurrentDirectory).Parent.Parent.Parent.FullName;
            Configuration = new ConfigurationBuilder()
                .AddJsonFile($"{rootPath}/testsettings.json").Build();
        }

        public void Dispose()
        {

        }
    }
}
