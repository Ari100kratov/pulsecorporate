﻿using Pulse.Calendar.Domain.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulse.Calendar.Domain.EntitiesDto
{
    /// <summary>
    /// Data Transfer Object (DTO) for representing a meeting.
    /// </summary>
    public sealed class MeetingDto : BaseEntityDto
    {
        /// <summary>
        /// Gets or sets the topic of the meeting.
        /// </summary>
        public required string Topic { get; set; }

        /// <summary>
        /// Gets or sets the description of the meeting.
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// Gets or sets the start time of the meeting.
        /// </summary>
        public required DateTime StartTime { get; set; }

        /// <summary>
        /// Gets or sets the end time of the meeting.
        /// </summary>
        public required DateTime EndTime { get; set; }

        public required Guid CreatorId { get; set; }

        /// <summary>
        /// Gets or sets the creator of the meeting.
        /// </summary>
        public required UserDto Creator { get; set; }

        public IEnumerable<Guid>? ParticipantsId { get; set; }

        /// <summary>
        /// Gets or sets the participants of the meeting.
        /// </summary>
        public required ICollection<UserDto> Participants { get; set; }
    }
}
