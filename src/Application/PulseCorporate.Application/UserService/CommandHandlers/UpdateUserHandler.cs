﻿using AutoMapper;
using MediatR;
using PulseCorporate.Application.UserService.Commands;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions.Administration;
using RabbitMQService.Interfaces;


namespace PulseCorporate.Application.UserService.CommandHandlers
{
    public class UpdateUserHandler : IRequestHandler<UpdateUserAsyncCommand, UserDto>
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IRabbitMQProducer<EmailDto> _rabbit;

        public UpdateUserHandler(IUserRepository userRepository, IMapper mapper, IRabbitMQProducer<EmailDto> rabbit)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _rabbit = rabbit;
        }

        public async Task<UserDto> Handle(UpdateUserAsyncCommand request, CancellationToken cancellationToken)
        {
            var email = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress")).Value;
            request.User.Email = email;

            var existUser = await _userRepository.GetByEmailAsync(email);

            if (existUser is not null)
            {
                request.User.Id = existUser.Id;
                _userRepository.Update(_mapper.Map<User>(request.User));
            }
            else
            {
                _userRepository.Add(_mapper.Map<User>(request.User));
            }
            
            await _userRepository.SaveChangesAsync(cancellationToken);

            _rabbit.SendMessage(new EmailDto ( email, "Update user", $"User changed old name - {existUser?.Name}, new name - {request.User.Name}; old surname - {existUser?.Surname}, new surname - {request.User.Surname}"));

            return request.User;
        }
    }
}
