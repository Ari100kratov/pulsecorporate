﻿using Calendar.API.Domain.Entities;

namespace Calendar.API.Application.Abstractions
{
    public interface IMeetingRepository : IRepository<Meeting>
    {
        Task<IEnumerable<Meeting>> GetMeetingsOfUserAsync(User user);
    }
}
