﻿using MediatR;
using PulseCorporate.Application.PostService.Commands;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions.Administration;
using RabbitMQService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Application.PostService.CommandHandlers
{
    public class DeletePostHandler: IRequestHandler<DeletePostAsyncCommand, Guid>
    {
        private readonly IPostRepository _postRepository;
        private readonly IRabbitMQProducer<EmailDto> _rabbit;

        public DeletePostHandler(IPostRepository postRepository, IRabbitMQProducer<EmailDto> rabbit)
        {
            _postRepository = postRepository;
            _rabbit = rabbit;
        }

        public async Task<Guid> Handle(DeletePostAsyncCommand request, CancellationToken cancellationToken)
        {
            var email = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress")).Value;

            var existRole = await _postRepository.GetByIdAsync(request.Id) ??
                throw new KeyNotFoundException($"Not found {nameof(Post)} with this id: {request.Id}");

            _postRepository.Delete(existRole);
            await _postRepository.SaveChangesAsync(cancellationToken);

            _rabbit.SendMessage(new EmailDto(email, "Delete post", $"Delete post id - {request.Id}"));

            return request.Id;
        }
    }
}
