﻿using Calendar.API.Application.Abstractions;
using Calendar.API.Domain.Entities;
using Calendar.API.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Calendar.API.Infrastructure.Implementation
{
    public sealed class MeetingRepository: BaseRepository<Meeting>, IMeetingRepository
    {
        /// <summary>
        /// Initializes a new instance of the MeetingRepository class.
        /// </summary>
        /// <param name="context">The application context.</param>
        public MeetingRepository(IApplicationContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Meeting>> GetMeetingsOfUserAsync(User user)
        {
            return await _entitySet.AsNoTracking().Where(x => x.Participants.Contains(user))
                .Include(c => c.Creator).Include(p => p.Participants).ToListAsync();
        }

        public override async Task<Meeting?> GetByIdAsync(Guid id, bool noTracking = true)
        {
            return noTracking ? await _entitySet.AsNoTracking().Include(p => p.Participants).FirstOrDefaultAsync(x => x.Id == id) :
                await _entitySet.Include(p => p.Participants).FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
