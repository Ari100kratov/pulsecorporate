﻿using AutoMapper;
using MediatR;
using Pulse.User.Application.UserService.Queries;
using Pulse.User.Domain.EntitiesDto;
using Pulse.User.Repositories.Abstractions.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulse.User.Application.UserService.QueriesHandlers
{
    public class GetUsersHandler : IRequestHandler<GetUsersAsyncQuery, IEnumerable<UserDto>>
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        public GetUsersHandler(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<UserDto>> Handle(GetUsersAsyncQuery request, CancellationToken cancellationToken)
        {
            return _mapper.Map<IEnumerable<UserDto>>(await _userRepository.GetAllAsync());
        }
    }
}
