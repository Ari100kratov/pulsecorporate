﻿using AutoMapper;
using MediatR;
using PulseCorporate.Application.RoleService.Queries;
using PulseCorporate.Application.UserService.Queries;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Application.UserService.QueriesHandlers
{
    public class GetUserByEmailHandler : IRequestHandler<GetUserByEmailAsyncQuery, UserDto>
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        public GetUserByEmailHandler(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<UserDto> Handle(GetUserByEmailAsyncQuery request, CancellationToken cancellationToken)
        {
            var email = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress")).Value;

            var existUser = await _userRepository.GetByEmailAsync(email);

            if (existUser is null)
            {
                var name = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname")).Value;
                var surName = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname")).Value;
                _userRepository.Add(new User { Name = name, Surname = surName, Email = email });
                await _userRepository.SaveChangesAsync(cancellationToken);

                existUser = await _userRepository.GetByEmailAsync(email);
            }            

            return _mapper.Map<UserDto>(existUser);
        }
    }
}
