﻿
using Microsoft.EntityFrameworkCore;
using Pulse.Calendar.Application.Interfaces;
using System.Reflection;

namespace Pulse.Calendar.Infrustructure.Pesrsistence
{
    public class PulseCalendarContext : DbContext, IApplicationContext
    {
        public PulseCalendarContext(DbContextOptions<PulseCalendarContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }
    }
}
