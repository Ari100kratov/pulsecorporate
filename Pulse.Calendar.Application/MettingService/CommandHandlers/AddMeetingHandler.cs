﻿using AutoMapper;
using Pulse.Calendar.Domain.Entities;
using Pulse.Calendar.Domain.EntitiesDto;
using Pulse.Calendar.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulse.Calendar.Application.MettingService.CommandHandlers
{
    public class AddMeetingHandler : IRequestHandler<AddMeetingAsyncCommand, MeetingDto>
    {
        private readonly IMeetingRepository _meetingRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IRabbitMQProducer<EmailDto> _rabbit;

        public AddMeetingHandler(IMeetingRepository meetingRepository, IUserRepository userRepository,
            IMapper mapper, IRabbitMQProducer<EmailDto> rabbit)
        {
            _meetingRepository = meetingRepository;
            _userRepository = userRepository;
            _mapper = mapper;
            _rabbit = rabbit;
        }

        public async Task<MeetingDto> Handle(AddMeetingAsyncCommand request, CancellationToken cancellationToken)
        {
            var email = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress")).Value;

            var existUser = await _userRepository.GetByEmailAsync(email, false);

            if (existUser is null)
            {
                var name = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname")).Value;
                var surName = request.UserClaims.First(c => string.Equals(c.Type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname")).Value;
                _userRepository.Add(new User { Name = name, Surname = surName, Email = email });
                await _userRepository.SaveChangesAsync(cancellationToken);

                existUser = await _userRepository.GetByEmailAsync(email);
            }

            request.Meeting.CreatorId = existUser!.Id;
            request.Meeting.ParticipantsId = request.Meeting.ParticipantsId!.Except(new Guid[] { existUser!.Id });

            var addMeeting = _mapper.Map<Meeting>(request.Meeting);

            var participants = await _userRepository.GetUsersById(request.Meeting.ParticipantsId!, false);
            addMeeting.Participants = participants;
            addMeeting.Participants.Add(existUser);

            var newMeeting = _meetingRepository.Add(addMeeting);
            await _meetingRepository.SaveChangesAsync(cancellationToken);

            _rabbit.SendMessage(new EmailDto(email, "Add meeting", $"Add meeting topic - {request.Meeting.Topic}, description - {request.Meeting.Description}"));

            newMeeting.Creator = existUser;
            newMeeting.Participants = participants;

            return _mapper.Map<MeetingDto>(newMeeting);
        }
    }
}
