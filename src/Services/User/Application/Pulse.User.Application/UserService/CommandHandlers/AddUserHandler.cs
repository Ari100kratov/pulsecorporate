﻿using AutoMapper;
using MediatR;
using Pulse.User.Application.UserService.Commands;
using Pulse.User.Domain.EntitiesDto;
using Pulse.User.Repositories.Abstractions.Interfaces;
using Pulse.User.Domain.Entities;

namespace Pulse.User.Application.UserService.CommandHandlers
{
    public class AddUserHandler : IRequestHandler<AddUserAsyncCommand, UserDto>
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public AddUserHandler(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<UserDto> Handle(AddUserAsyncCommand request, CancellationToken cancellationToken)
        {
            _userRepository.Add(_mapper.Map<Pulse.User.Domain.Entities.User>(request.User));

            await _userRepository.SaveChangesAsync(cancellationToken);

            return request.User;
        }
    }
}
