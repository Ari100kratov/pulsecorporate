﻿using AutoMapper;
using Pulse.User.Domain.EntitiesDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pulse.User.Application.Mapping
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDto, Domain.Entities.User>()
                //.ForMember(x => x.Meetings, map => map.Ignore())
                //.ForMember(x => x.AddMeetings, map => map.Ignore())
                .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember is not null));
            CreateMap<Domain.Entities.User, UserDto>();
        }
    }
}
