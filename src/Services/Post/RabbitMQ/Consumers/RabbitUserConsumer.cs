﻿using AutoMapper;
using Post.API.Application.Abstractions;
using Post.API.Domain.Entities;
using Post.API.Domain.EntitiesDto;
using Post.API.RabbitMQ.Interfaces;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Text.Json;

namespace Post.API.RabbitMQ.Consumers
{
    public class RabbitUserConsumer:IRabbitUserConsumer
    {
        private readonly IConnection _connection;
        private readonly ConnectionFactory _connectionFactory;
        private readonly IModel _model;
        private readonly IConfiguration _configuration;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public RabbitUserConsumer(IUserRepository userRepository, IMapper mapper)
        {
            _mapper = mapper;
            _userRepository = userRepository;
            _configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();
            _connectionFactory = new ConnectionFactory() { HostName = _configuration["RabbitMQServices:HostName"], UserName = _configuration["RabbitMQServices:UserName"], Password = _configuration["RabbitMQServices:Password"] };
            _connection = _connectionFactory.CreateConnection();
            _model = _connection.CreateModel();
            _model.QueueDeclare(queue: _configuration["RabbitMQServices:QueueNameUser"],
                               durable: false,
                               exclusive: false,
                               autoDelete: false,
                               arguments: null);
        }

        public void Execute()
        { 
            var consumer = new EventingBasicConsumer(_model);
            consumer.Received += (ch, ea) =>
            {
                var body = ea.Body.ToArray();
                var userDto = JsonSerializer.Deserialize<UserDto>(Encoding.UTF8.GetString(body));
                _userRepository.Add(_mapper.Map<User>(userDto));
                _userRepository.SaveChangesAsync();

                _model.BasicAck(ea.DeliveryTag, false);
            };

            _model.BasicConsume(queue: _configuration["RabbitMQServices:QueueNameUser"],
                                                autoAck: false,
                                                consumer: consumer);

        }
    }
}
