﻿using Serilog;
using System.Text;

namespace Post.API.Middleware
{
    public class LoggingMiddleware
    {
        private readonly RequestDelegate _next;

        public LoggingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            // Логирование входящего запроса
            var originalRequestBodyStream = context.Request.Body;
            var requestBodyStream = new MemoryStream();
            await context.Request.Body.CopyToAsync(requestBodyStream);
            requestBodyStream.Seek(0, SeekOrigin.Begin);

            var url = context.Request.Path;
            var requestBodyText = await new StreamReader(requestBodyStream).ReadToEndAsync();
            requestBodyStream.Seek(0, SeekOrigin.Begin);
            context.Request.Body = requestBodyStream;

            // Исключение JWT из заголовков перед логированием
            var headers = context.Request.Headers;
            var headersLog = new StringBuilder();
            foreach (var header in headers)
            {
                if (header.Key != "Authorization" && !header.Key.StartsWith("Bearer "))
                {
                    headersLog.AppendLine($"{header.Key}: {header.Value}");
                }
            }

            Log.ForContext("RequestBody", requestBodyText)
                .ForContext("Url", url)
                .ForContext("RequestHeaders", headersLog.ToString())
                .Information("Incoming request {Method} {Url}", context.Request.Method, url);

            // Логирование исходящего ответа
            var originalResponseBodyStream = context.Response.Body;
            using var responseBodyStream = new MemoryStream();
            context.Response.Body = responseBodyStream;
            var headersRes = context.Response.Headers;

            await _next(context);

            responseBodyStream.Seek(0, SeekOrigin.Begin);
            var responseBody = await new StreamReader(responseBodyStream).ReadToEndAsync();
            responseBodyStream.Seek(0, SeekOrigin.Begin);
            await responseBodyStream.CopyToAsync(originalResponseBodyStream);

            Log.ForContext("ResponseBody", responseBody)
                .ForContext("ResponseHeaders", headersRes.ToString())
                .Information("Outgoing response {StatusCode}", context.Response.StatusCode);
        }
    }
}
