﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using PulseCorporate.Application.Interfaces;
using PulseCorporate.Domain.Entities;
using System.Reflection;

namespace PulseCorporate.Infrastructure.Persistence;
public class PulseCorporateContext : DbContext, IApplicationContext
{
    public PulseCorporateContext(DbContextOptions<PulseCorporateContext> options) : base(options) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        base.OnModelCreating(modelBuilder);

        //modelBuilder.Entity<Role>().HasData(SeedData.Roles);
        //modelBuilder.Entity<Post>();
        //modelBuilder.Entity<Meeting>();
        //modelBuilder.Entity<User>();
    }   
}
