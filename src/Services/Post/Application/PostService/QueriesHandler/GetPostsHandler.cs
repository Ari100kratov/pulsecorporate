﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Caching.Distributed;
using Post.API.Application.Abstractions;
using Post.API.Application.PostService.Queries;
using Post.API.Domain.Entities;
using Post.API.Domain.EntitiesDto;
using Post.API.RabbitMQ.Interfaces;
using System.Text.Json;

namespace Post.API.Application.PostService.QueriesHandler
{
    public sealed class GetPostsHandler : IRequestHandler<GetPostsAsyncQuery, IEnumerable<PostDto>>
    {
        private readonly IPostRepository _postRepository;
        private readonly IMapper _mapper;
        private readonly IDistributedCache _distributedCache;
        private readonly ILogger<GetPostsHandler> _logger;
        private readonly IRabbitEmailProducer<EmailDto> _rabbit;

        public GetPostsHandler(IPostRepository postRepository, IMapper mapper, IDistributedCache distributedCache, ILogger<GetPostsHandler> logger, IRabbitEmailProducer<EmailDto> rabbit)
        {
            _logger = logger;
            _postRepository = postRepository;
            _mapper = mapper;
            _distributedCache = distributedCache;
            _rabbit = rabbit;
        }

        public async Task<IEnumerable<PostDto>> Handle(GetPostsAsyncQuery request, CancellationToken cancellationToken)
        {
            _rabbit.Send(new EmailDto("pulsecorporate@mail.ru", "Get posts", "description"));
            string serializedPosts = await _distributedCache.GetStringAsync("posts:all");
            if (serializedPosts != null)
            {
                var deserializedPosts = JsonSerializer.Deserialize<IEnumerable<Posts>>(serializedPosts);
                return deserializedPosts.Select(_mapper.Map<PostDto>);
            }
            var posts = await _postRepository.GetAllAsync();
            if (posts != null)
            {
                await _distributedCache.SetStringAsync(
                    key: "posts:all",
                    value: JsonSerializer.Serialize(value: posts),
                    options: new DistributedCacheEntryOptions
                    {
                        SlidingExpiration = TimeSpan.FromSeconds(10),
                    });
            }
            return posts.Select(_mapper.Map<PostDto>);
        }

    }
}
