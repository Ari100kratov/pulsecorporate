﻿using AutoMapper;
using MediatR;
using PulseCorporate.Application.RoleService.Commands;
using PulseCorporate.Domain.Entities;
using PulseCorporate.Domain.EntitiesDto;
using PulseCorporate.Repositories.Abstractions.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseCorporate.Application.RoleService.CommandHandlers
{
    public class AddRoleHandler : IRequestHandler<AddRoleAsyncCommand, RoleDto>
    {
        private readonly IRoleRepository _roleRepository;
        private readonly IMapper _mapper;

        public AddRoleHandler(IRoleRepository roleRepository, IMapper mapper)
        {
            _roleRepository = roleRepository;
            _mapper = mapper;
        }

        public async Task<RoleDto> Handle(AddRoleAsyncCommand request, CancellationToken cancellationToken)
        {
            _roleRepository.Add(_mapper.Map<Role>(request.Role));
            await _roleRepository.SaveChangesAsync(cancellationToken);

            return request.Role;
        }
    }
}
